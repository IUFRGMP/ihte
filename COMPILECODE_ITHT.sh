 
 #this script compiles the code for iterated Hilbert Transfrom
 #CASE1: passive normalization, passive cleansing
 #CASE2: active spline passive cleansing
 #CASE3: passive spline passive cleansing
 #CASE4: active spline active cleansing
 #CASE5: active normalization passive cleansing
 #CASE6: active normalization active cleansing

 HEADERPATH='../HEADERFILES_ITHT_LENGTH/'
 CODEPATH='../CODE_ITHT_LENGTH/'
 
 g++ $HEADERPATH/Main.h $CODEPATH/Cleansing.cpp $CODEPATH/HilbertTransform.cpp $CODEPATH/PhaseCalculation.cpp $CODEPATH/Preprocessing.cpp $CODEPATH/Utilities.cpp $CODEPATH/Amplitude.cpp $CODEPATH/Main.cpp -std=c++11 -O -o ITHT_TEST_SIGNAL_SPLINE_TIMEMEASURE_ERROR_MAXPOINTS_X_CLEANSING_SG_FILTER_SIG_pars_SG_FILTER_PHI_pars_CASE2
