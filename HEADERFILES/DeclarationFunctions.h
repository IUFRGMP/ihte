// ### FUNCTION DECLARATION FOR IT. HILBERT TRANSFROM ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

//### DECLARATIONS IN CHRONOLOGICAL ORDER OF THE CODE ###//
//Artificail Data Geenration
DOUB phaseFunction(DOUB &x, DOUB &r, DOUB &initValue, Vectordata &Data);
void OMEG(vector<DOUB> y, vector<DOUB> &dydx, int N, DOUB &t);
void rk4(void (*derives)(vector<DOUB> y, vector<DOUB> &dydx, int N, DOUB &t), vector<DOUB> &y, int Nsim, DOUB &t, int N, DOUB dt);
vector<DOUB> noiseGauss(DOUB &mean, DOUB &sigma);
DOUB Signal(DOUB &phase, DOUB &t, DOUB &amp, DOUB &omeg, DOUB &Gnoise);

#ifndef fromfile
void dataGenerator1(DOUB (*Signal)(DOUB &phase, DOUB &t, DOUB &amp, DOUB &omeg, DOUB &Gnoise), DOUB (*phase)(DOUB &t, DOUB &r, DOUB &intiValue, Vectordata &Data), Vectordata &Data, DOUB &t, DOUB &omeg, DOUB &amp, DOUB &noiseamplitude, DOUB &sigma, DOUB &r, DOUB &in, DOUB &intiValue);
#else
void extractor(Vectordata &Data, string &datafile);//, const int &Ntime, const int &Nint);
#endif

//Smoothing of noise
void SawitzkyGolayFilterQ(vector<DOUB> &data2, vector<DOUB> &time, int &WS, int &N, int &M, int d);// Sawitzky-Golay filter with variable point and degree and variable grid
void SawitzkyGolayFilter25(vector<DOUB> &data2, int &WS, int &N, int &M, DOUB in);//Sawitzky-Golay filter with 9 to 25 points
int checkIndex(int &index, int &lower, int &upper, int &shift);
void setConvolveSG(vector<DOUB> &cof, int &M);
void movingAverage(vector<DOUB> &data2, int &WS, int &N);//moving average with Window size WS 

//Maximum interpolation
DOUB L2(DOUB &y1, DOUB &y2, DOUB &y3, DOUB &t1, DOUB &t2, DOUB &t3, DOUB &t);
DOUB findmax(vector<DOUB> &data, int &Nint2);
void LAGRANGE_MAX(DOUB &f0,DOUB &f1,DOUB &f2,DOUB &f3,DOUB &f4,DOUB &f5,DOUB &X0,DOUB &X1,DOUB &X2,DOUB &X3,DOUB &X4,DOUB &X5,DOUB &interTime, DOUB &dataval);//for quintic maximum point interpolation
DOUB L5(DOUB &f0,DOUB &f1,DOUB &f2,DOUB &f3,DOUB &f4,DOUB &f5,DOUB &X0,DOUB &X1,DOUB &X2,DOUB &X3,DOUB &X4,DOUB &X5,DOUB &x);
void InitialDataCopy(Vectordata &Data, DOUB &in, int &Nint2, DOUB &thresholdext);//for use of lengt as integration grid -> omits interpolated points but writes out the points to a vector as in MaxInterpolator2

//Preparation Works befor simulation/analysis starts
void Structresize(Vectordata &Data, int &M);
void copier(vector<DOUB> vec, vector<DOUB> &vec2);
void ERROR1(vector<DOUB> &data, vector<DOUB> &grid, int &Nint2, Vectordata &Data, DOUB &L);
void linearFit(DOUB &m, DOUB &n, vector<DOUB> &data, vector<DOUB> &timeGrid, int &start, int &end, DOUB &stepadjust);
void Calc_Modulation(vector<DOUB> &modulation, vector<DOUB> &phase, vector<DOUB> &timeGrid, int start, int end);
DOUB INTEGRATED_STANDART_DEVIATION(vector<DOUB> &data1, vector<DOUB> &data2, vector<DOUB> &Grid, Vectordata &Data, int &Nint2);
void Minimizer_length(Vectordata &Data, int p, DOUB &in, int &Nint2);//if length is used as hilbert transform grid
void preparation(Vectordata &Data, int &M, int &startpoint, int &endpoint, DOUB &r, int &Nint2);
DOUB LAGRANGE_N(int &degree, vector<DOUB> &newtimes, vector<DOUB> &newdata, DOUB &x);
void ERROR2(vector<DOUB> &data, vector<DOUB> &grid, int &Nint2, Vectordata &Data, DOUB &L);

//Hilbert transform
DOUB MeanCalc(Vectordata *Data); 
DOUB MeanCalc_Length(Vectordata *Data);
void Hilbert2(Vectordata *Data, int p, DOUB in);
void Hilbert3(Vectordata *Data, int p, DOUB in); 
void Hilbert_Simpson(vector<DOUB> &hilberttrafo, vector<DOUB> &data, vector<DOUB> &Grid, vector<DOUB> &proofGrid, int p, DOUB in);
void Hilbert4(Vectordata *Data, int p, DOUB in);

//Derivative embedding 
void SG_derive(vector<DOUB> &hilberttrafo, vector<DOUB> &data, vector<DOUB> &Grid);

//Phase calculation
void lengthCalculator(Vectordata *Data);
void lengthCalculator_quadratic(Vectordata *Data);
void setPeriodPhase2(Vectordata *Data, DOUB &deriveBegin, DOUB &deriveEnd);
DOUB cubicSpline(DOUB L, DOUB Li, DOUB Lj, DOUB phasei, DOUB phasej, DOUB phase2i, DOUB phase2j);
void phaseCalculator5(Vectordata *Data, int p, DOUB &in);
void LengthOptimizer(Vectordata &Data, DOUB &L, int &Nint2);
void phaseCalculator_Length(Vectordata *Data, DOUB &L, int p, DOUB &in);

//Amplitude Reconstruction
DOUB LAGRANGE_N(int &degree, vector<DOUB> &newtimes, vector<DOUB> &newdata, DOUB &x);
void Demodulator(vector<DOUB> &Varydata, vector<DOUB> &data3, vector<DOUB> &proofGrid, vector<DOUB> &data, Vectordata &Data, DOUB &in, int Nint2, DOUB &thresh);
void ConstructAmplitude(Vectordata &Data, DOUB &L, DOUB &in, int &Nmodes);


//Cleansing
void FourierTrasform(Vectordata *Data, int &start, int &end, ofstream &fouriertrans);
void FourierTrasform2(Vectordata *Data, int &start, int &end, ofstream &fouriertrans);
void FourierTrasform3(vector<DOUB> &proofGrid, vector<DOUB> &evenpart, vector<DOUB> &oddpart, vector<DOUB> &helpdata, vector<DOUB> &phase, int &start, int &end, ofstream &fouriertrans, DOUB &divider);
DOUB TransfromSum(Vectordata *Data, DOUB x);
void phaseTransform4(Vectordata *Data, ofstream &transform);
void iterateTransform(Vectordata *Data, int &startpoint, int &endpoint, DOUB &in, int p);
void Cleansing_EMD(Vectordata *Data, int &startpoint, int endpoint, DOUB &in, int p);
void Calc_derivative(Vectordata *Data, vector<DOUB> &derive_Phase, vector<DOUB> &data, vector<DOUB> &proofGrid, int a, int b, int c);
DOUB MeanCalc2(vector<DOUB> &data, vector<DOUB> &time, int &start, int &end);
void Pdist(Vectordata &Data, int &start, int &end, DOUB &in, int p);
void mysort(vector<DOUB> &x, vector<DOUB> &y);
void Cleansing_for_length_phases(Vectordata &Data, DOUB &in, int p);


//print of data
void dataprint(Vectordata *Data, int p, DOUB &in, int &Nint2, const int &n, string path_to_folder);
