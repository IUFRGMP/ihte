// ### CONSTANT DEFINITIONS FOR THE IT. H. TRANSFROM  ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

//### CONSTANTS OF ITERATION ###//
//const int NHi();              //Number of Hilbert iterates - 1 NOTE: No longer in use iterate number is given as function argument
const int n(2);                 //modulo number for reduction of plotting data
const int Ncleanse(1);          //Number of cleansing steps performed to reduce periodic modulations (not necessary)
const DOUB threshold(0.7);      //threshold for maxinterpolation. Above threshold*globalmximum a point is counted as a maximum ->amplitude sensitivity NOTE: Now given as input argument

//### CONSTANTS THAT ARE ONLY NEEDED IF DATA IS GENERATED ARTIFICIALLY (DATA NOT FROM FILE) ###//
#ifndef fromfile
const int Ntime(60000);        //number of time points
const int Nint(6000);          //fixed number of time points at boundaries for output cutoff and find max function
const DOUB dt(0.02);           //time step
//const DOUB r(0.25);          //making phase modulations faster NOW AN INPUT PARAMETER
const int Modes(10);           //modes of the Signal
#endif


