// ### MAIN HEADER FOR THE ITERATED HILBERT TRANSFROM ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

//### BASIC SELF-DEFINED HEADERS ###//
#include "CompileOptions.h"         //gives compile options
#include "StructDefinitions.h"      //defines the data structure
#include "Constants.h"              //defines necessary constants
#include "DeclarationFunctions.h"   //declares functions
#include <omp.h>                    //for parallelism of some loops openMP
#include "eigen/Eigen/Dense"        //for linear algebra 

using namespace Eigen;

typedef Matrix<DOUB,Dynamic,Dynamic> MatD;
typedef Matrix<DOUB,Dynamic,1> VecD;



