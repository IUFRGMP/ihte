// ### STRUCTS FOR THE ITERATED HILBERT TRANSFROM     ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

//### Dtype for precission controlle ###//
typedef long double DOUB ;

const DOUB pi(atan(1.0)*4.0); //pi

//### DEFINITION OF MAIN DATA STRUCTURE ###
typedef struct Vectordata
{

  //### 2 amplitudes and 2 frequencies for artificial phase data ###//
  DOUB a;
  DOUB b;
  DOUB f;
  DOUB g;
  int sl; //signal length
  DOUB dt; //time step

  //### Basic data ###//
  vector<DOUB> data2;        //raw input data
  vector<DOUB> data3;        // TODO:not used do a copy of data and use it for Hilbert transform with correction
  vector<DOUB> timeGrid;     //raw time line
  vector<DOUB> anaphase;     //analytic phase (if known)
  vector<DOUB> anafreq;      //analytic phase derivative (if known)

  //### Parsing of data ###// 
  int xcol;                  //data column index in data file counted from 1 (0 is time)
  int phasecol;              //true phase column in data file counted from 1 (0 is time, actuay the first col index is 3 for one oscillator)
  int derivecol;             //true phase derivative column in data file counted from 1 (0 is time) 

  //### Interpolation vectors ###//
  vector<DOUB> newtimes;     //interpolated maxtimes of data2
  vector<DOUB> newdata;      //interpolated data of data 2
  vector<int>  maxindex;     //index of interpolation point in data
  vector<DOUB> data;         //data with maxpoints from data2
  vector<DOUB> Grid;         //first step: time grid later phase grid
  vector<DOUB> proofGrid;    //time grid (remains unchanged)
  vector<DOUB> frequency;    //frequency of analytic phase if artificial signal is generated (dataGen)
  vector<DOUB> anaphase2;    //analytic phase with interpolated points at values of newtimes
  vector<DOUB> anafreq2;      //true external phase derivative given from external method not SG filtering (see derive_APhase)
  
  //### Envelope ###//
  vector<DOUB> originalData; //copy of data remains unchanged
  vector<DOUB> Varydata;     //difference of amplitude constant data and raw data
  vector<DOUB> envelope;     //contains the Hilbert envelope sqrt(data^2+hilberttrafo^2)
  vector<DOUB> GridAmp;      //separate amplitude phase first step time afterwards separate phase
   
  //### Transform Loop ###//
  vector<DOUB> hilberttrafo; //iterated hilbert transform of amplitude conastant wave form
  vector<DOUB> htrafoVary;   //iterated hilbert transform of amplitude deviation wave form
  vector<DOUB> length;       //length of embedding
  vector<DOUB> phase;        //calculated phase
  vector<DOUB> periodphase;  //vector of interpolation phases for spline phase.  has length of maxtimes+2 boundar. points
  vector<DOUB> periodphase2; //vector of phase derivatives for spline phase, same length as periodphase
  vector<DOUB> periodlength; //vector of length values at maximum points -> spline phase for interpolation
  vector<DOUB> stretchedPhase;//cleansed phase 
  vector<DOUB> Error;         //periodicity error
  vector<DOUB> modulation;    //modulation of phase (asymptotic phase maybe others later)  


  //### CLEANSING ###//
  vector<DOUB> helpdata;     //set to unity but is just a placeholder for data in the cleansing
  vector<DOUB> evenpart;     //even phase modes used in the cleansing (length = M*length maxtimes)
  vector<DOUB> oddpart;      //odd phase modes used in the cleansing
  vector<DOUB> mod_proxip;   //MAYBE used int the EMD for improved cleasnig as the starting function
  vector<DOUB> mod_Cproxip;  //MAYBE used int the EMD for improved cleasnig as the starting function
  vector<DOUB> derive_APhase; //used in the EMD for improved cleansing as the derivative function or output 
  vector<DOUB> derive_PPhase; //used in the EMD for improved cleansing as the derivative function or output
  vector<DOUB> derive_CPhase; //used in the EMD for improved cleansing as the derivative function or output

}Vectordata;
