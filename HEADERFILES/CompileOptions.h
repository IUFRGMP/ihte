// ### COMPILER DEFINITIONS FOR THE IT. H. TRANSFROM  ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //


//#define debugging                 // enables debugging Hilbert trafo, phase calculation
//#define mono                      // enables mono component signal function in function declaration and definition
//#define fromfile                    // enables read from file
//#define modulous                  // enables modulous in the main data output to reduce the amount of data.
//#define measuretime               //enables time measurement of processes
#define debuggingmaxinterpolator    //enables interpolated points to plot and list of index in data
//#define debuggcleansing           //for debugging of the iterated cleasing --->>> increased output size
#define Hilbert_simpson             //use of Simpson rule for Hilbert integral, symetric integration arround singularity --> to be done
                                    //if commented out: trapezoidal method 
//#define debugging_Hilbert_simpson //debug simpson Hilbert method
//#define length_calc_quadratic     //use of quadratic curve length estimation, if commented out: linear polygons
#define calc_intermediate_phases    //calculates intermediate phases iterates in the procedure
//#define Phase_from_Length         //calculate Phase from the Length of the embedding with minimizing period Length 
#define splinePhase                 //enables cubic spline phase calculation -> exactness limited: maxpoints are not interpolated!
//#define lengthPhase               //enables linear spline phase calculation-> exactness limited: maxpoints are not interpolated!
//#define arctanPhase               //enables arctan phase calculation       -> exactness limited: maxpoints are not interpolated!
//#define debuggingPhase            //enables debuggin output for phase calculation
//#define debuggingDatagen          //plot the generated data before maximum interpolation
//#define frequencyUse              // define frequency of phase instead of phase
//#define Addnoise                  //Add gausian white noise to the data
//#define minimizer                 //enable plot of period error depending on minimizing period Length for each iteration step
#define smoothing                   //enables Savitzky-Golay filter for X(t) and true phase
#define anafreq_use                 //instead of Savitzky-Golay derivative uses true derivative given from file and smoothes it like X(t)
//#define AmplitudeReconstruction   // enables reconstruction of amplitude/envelope
//#define Demodulation              //enables construction of upper envelope with cubic spline
//#define debuggamplitude           // define debugging of amplitude reconstructi
//#define write_one_intermediate_data //enables plotting of one iteration step at integer p to be defined as input argument
//#define no_output                 //allows to suppress output of all main data files 
                                    //(still output of Error, maxinterpolation file)
//#define parametric_phase          //performs fourier analysis of phase modulation TODO: find a way to subtract with 10-8 precission the mean growth of phase and perform analysis of the modulation only
#define Phase_distribution          // enables visualization of the oscillator distribution from cleansing on 2pi interval

//The combination of IHTE and DPT can be carried out in different ways! The following cases refere to different ways of iterative reuse of the phase. The code may be addapted to the different options below

//#ifdef case5 // copy normalized phase to grid -> case 5
//#ifdef case1 // copy length to grid case 1-> normalization is passive (active but not needed), cleansing is passive
//#ifdef case6 // case 6 -> active normalization, active cleansing
#define case2   // copy intermediate phase to grid; case 2-> spline is active but cleansing is passive
//#ifdef case3 // case 3-> interpolate maxima but use unnormalized length as phase, spline and cleansing remain passive    
//#ifdef case4 // copy intermediate stretched phase to grid, case 4-> spline and cleansing active

