# IHTE

A latest version of the iterative Hilbert transform embedding for extraction of phase modulation from (nearly) phase modulated signals.
Input: time series data 
Output: Maximum points file, Error file, Hilbert transform file, debug+time, phase probability distribution density functions

The method can handle phase modulated wideband signals of a wide range of complexity limited mainly by the resolution of data only. In presence of amplitude variation, the convergence is weaker 
the method is based on a publication in SIGNAL PROCESSING -> find arxiv at: https://arxiv.org/abs/1901.08774

For compilation of the code the Eigen library may be placed in the Headerfiles folder.

The intended directory structure is:

home-> PROGRAMMING -> HEADERFILES, CODE, EXPERIMENTS-> COMPILECODE_ITHT.sh

The iterated hilbert transform is utilised in the following way:

./ITHT_FILES_SPLINE_TIMEMEASURE_ERROR_MAXPOINTS_X_CLEANSING_SG_FILTER_SIG_pars_SG_FILTER_PHI_pars_CASE2 - run number - input_file.dat - r - a - w2 - m1 - m2 - #of iterates for IHTE - deg - rep - np - modes - step - thresh - deg2 - rep2 - np - data col - phase col - path to output directory &

r: modulation paramter for artificial test signals ;---
a: amplitude modulation paramter for artificail test signal ;---
w2: modulation frequency of amplitude for artificial test signal ;---
m1: mean value of additive white gausian noise for artificial test signal ;---
m2: standart deviation of additive white gausian noise ;---
deg: polynomial degree of Savitzky-Golayfilter for data smoothing ;--- 
rep: repetition steps of SG filter for data smoothing ;---
np: number of window points for SG filter and data smoothing ;--- 
modes: numer of Fourier modes for Protophase to phase transformation ;---  
step: if specified by compiler this determines an intermediate step of iteration to be printed to a file while other files are not printed ;---
thresh: threshold above which a maximum in the time series is counted as a marker event ;--- 
deg2: degree of SG filter for instantaneous frequency estimation ;---
rep2: repetition steps for SG filter for instantaneous frequency estimation ;--- 
np: number of points for SG filter for instantaneous frequency estimation ;---
data col: specifies the column of data if data file is given ;---
phase col: specifies the column of data where a reference phase function is given (if not known time column may be used) ;---
path to output directory: Specifies the path (not the file name!) where output should be written to.