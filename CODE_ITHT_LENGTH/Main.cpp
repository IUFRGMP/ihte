#include "../HEADERFILES_ITHT_LENGTH/Main.h"

int main(int argc, char* argv[])
{
  //### GET ARGVALS ###//
  DOUB in = strtod(argv[1],NULL);                        // distinguish file parameter , argv[0] own name
  string datafile = argv[2];                             // file if readed
  DOUB r=strtod(argv[3],NULL);                           // modulation parameter
  DOUB amp=strtod(argv[4],NULL);                         // modulation parameter for amplitude
  DOUB romeg=strtod(argv[5],NULL);                       // modulation parameter for amplitude frequency
  DOUB noiseamp = strtod(argv[6],NULL);                  // noisestrength of additive noise, mean of noise
  DOUB sigma = strtod(argv[7],NULL);                     // noise standart deviation of additive noise
  int NHi2 = strtod(argv[8],NULL);                       // Number of Iterates for Hilbert transform
  int WS = int(strtod(argv[9],NULL));                    // polynomial degree (Sawitzky-Golay) window size (moving average)
  int N = int(strtod(argv[10],NULL));                    // repeating of Savitzky-Golay smoothing
  int MM = int(strtod(argv[11],NULL));                   // which SG filter? Permited are MM=9,11,13,19,25 points
  int Nmodes = int(strtod(argv[12],NULL));               // How many modes for cleansing?
  int Stepprint = int(strtod(argv[13],NULL));            // Which iteration step do you want to print
  DOUB thresholdext = DOUB(strtod(argv[14],NULL));       // Determines which peak hight triggers interpolation of maximum
  int a = int(strtod(argv[15],NULL));                    // polynomial degree
  int b = int(strtod(argv[16],NULL));                    // repetition
  int c = int(strtod(argv[17],NULL));                    // number of points in smoothing window
  int xcol = int(strtod(argv[18],NULL));                 // choose correct data column
  int phasecol = int(strtod(argv[19],NULL));             // choose correct column of corresponding true phase
  int derivecol = int(strtod(argv[20],NULL));            // choose correct column of corresponding true phase derivative
  string path_to_folder = argv[21];                      // give path to write the data to
  DOUB pv1 = strtod(argv[22],NULL);                      //two amplitudes a
  DOUB pv2 = strtod(argv[23],NULL);                      // b
  DOUB pv3 = strtod(argv[24],NULL);                      // and two frequencies f
  DOUB pv4 = strtod(argv[25],NULL);                      // and g
  int sl = strtod(argv[26],NULL);                        // number of points (artificail data)
  DOUB sdt = strtod(argv[27],NULL);                      // time step (artificial data)

  //### DEFINE FILE FOR PERIOD ERROR MINIMUM ###//
  std::stringstream deb4; deb4 << path_to_folder << "TransError" << argv[1] << ".dat";
  std::string name04 = deb4.str(); ofstream errorfile(name04,ios::out);
  errorfile.setf(ios::scientific); errorfile.precision(15);

  //### MEASURE TIME ###//  
#ifdef measuretime
  std::stringstream deb3; deb3 << path_to_folder << "ProcessTime" << argv[1] << ".dat";
  std::string name03 = deb3.str(); ofstream timesfile(name03,ios::out);
  timesfile.setf(ios::scientific); timesfile.precision(15);
  clock_t t1,t2, starttime;
  t1=clock();
  starttime=clock();
#endif
  
 //### DEFINE VECTOR CONTAINERS ###//
 Vectordata Data;
 Data.xcol=xcol;Data.phasecol=phasecol;Data.derivecol=derivecol;
 
 //### set artificial phase modes (if needed) ###//
 Data.a = pv1;
 Data.b = pv2;
 Data.f = pv3;
 Data.g = pv4;
 Data.sl = sl;
 Data.dt = sdt;

 //### GENERATION OF DATA ###//
  DOUB t(0.0);
#ifndef fromfile
  int Nint2 = int(0.1*sl);// Nint;
  DOUB initValue(0.0);
  dataGenerator1(Signal, phaseFunction, Data, t, romeg, amp, noiseamp, sigma,r, in,initValue); //generate the data
#else
  extractor(Data, datafile);//, Ntime, Nint);
  int Nint2 = int(DOUB(Data.data2.size())*0.1);//determine boundary cutoff as first and last 10% of time series
  cout << "cutoff= " << Nint2 << "\n";
#endif

  //SMOOTHING OF DATA// 
#ifdef smoothing
  SawitzkyGolayFilterQ(Data.data2, Data.timeGrid, WS, N, MM, 0);
  SawitzkyGolayFilterQ(Data.anaphase, Data.timeGrid, WS, N, MM, 0);//before a,b,c,0) as final paramters seems to make more sense to use WS,N,MM instead

#endif
    
  //### INTERPOLATE MAXIMA FOR CLEANSING ###//
  InitialDataCopy(Data, in, Nint2,thresholdext);  
  
  Nint2 = int(DOUB(Data.data.size())*0.1);//since maximum points are added: timeseries is longer -> new Nint
  
  //### RESIZE VECTORS IN DATA STRUC , SAVE TIME LINE , SAVE ANALYTIC PHASE , DEFINE PRINTING CONSTANTS ###//
  //### MEASURE TIMES, SET FIRST APPROXIMATION OF AMPLITUDES                                            ###//
  int M(Nmodes), startpoint(0), endpoint(0), count(0), num(0), nummax(0), p(0);
  DOUB L(0.0); //average period
  
  preparation(Data, M, startpoint, endpoint, r, Nint2);
#ifdef measuretime
  t2=clock();
  timesfile << "Preprocessing" << (DOUB)(t2-t1)/CLOCKS_PER_SEC << "\n";
#endif
  
  //### CALCULATE PERIOD ERROR IF ASYMPTOTIC PHASE IS USED (AMPLITUDE MODULATION LEVEL) ###//
  L=2.0*pi;
  ERROR2(Data.data, Data.anaphase2, Nint2, Data, L);//period error if analytic phase is used 
  DOUB anaVaraiance_data(Data.Error[0]), anaNorm_data(Data.Error[1]);

  ERROR1(Data.data, Data.anaphase2, Nint2, Data, L);//period error if analytic phase is used 
  DOUB anaErr(Data.Error[0]);
  L=0.0;

  //### CALCULATE ORIGINAL MODULATION OF THE TRUE PHASE AND ITS DERIVATIVE ###//
  Calc_Modulation(Data.modulation, Data.anaphase2, Data.proofGrid, Data.maxindex[2], Data.maxindex[Data.maxindex.size()-3]);
  //int a(4), b(4), c(25);//SG smoothing for variable grid derivative
#ifdef anafreq_use
  SawitzkyGolayFilterQ(Data.anafreq2, Data.timeGrid, WS, N, MM, 0);//if true frequency is available smoothe directly with SG paramters for data
  copier(Data.anafreq2, Data.derive_APhase);//for further use of smoothed true phase, copy it to derive_APhase
#else
  Calc_derivative(&Data, Data.derive_APhase, Data.anaphase2, Data.proofGrid, a, b, c);//else, use true phase and Sg derivative
#endif
  //calculate normalization for Derivative STD in Errorfile
  DOUB N_derive(INTEGRATED_STANDART_DEVIATION(Data.derive_APhase, Data.helpdata, Data.proofGrid, Data, Nint2));
    
  //### ##################### ###//
  //### ITERATION LOOP STARTS ###//
  //### ##################### ###//
  cout << "start iteration loop \n";
  int abord=0;
  for(p=0;p<NHi2;p++)
   {       
    //### HILBERT TRAFO ###//       
#ifdef measuretime 
    t1=clock();
#endif

#ifndef Hilbert_simpson
    Hilbert3(&Data, p, in);      //NOTE: Version 2,3,4 work, Version 4 is slower, best is 3 all are trapezoidal
#else
    
    Hilbert_Simpson(Data.hilberttrafo,Data.data,Data.Grid,Data.proofGrid,p,in);//TODO: possible improvment of integration

#endif
    
#ifdef measuretime
    t2=clock();
    timesfile << "Hilbertransf"<< p << " "  << (DOUB)(t2-t1)/CLOCKS_PER_SEC << "\n";
#endif
    
    //### CALCULATE LENGTH ###//
#ifndef length_calc_quadratic
    lengthCalculator(&Data);           //NOTE: linear rectification of the curve (stable)
#else
    lengthCalculator_quadratic(&Data); //NOTE: quadratic interpolation and integration (problem with
                                       //      large modulations
#endif
    
     cout << "calculate phase \n";
 #ifdef calc_intermediate_phases
     //### CALCULATE PHASE ###//
 #ifndef Phase_from_Length
     phaseCalculator5(&Data, p, in);
     L=2.0*pi;   
     //### CALCULATE ERROR OF TRANFORM ###//
     ERROR1(Data.data, Data.Grid, Nint2, Data, L); //for the moment if Hilbert phase is used
 #else
     LengthOptimizer(Data,L, Nint2);
     phaseCalculator_Length(&Data, L, p, in);
 #endif
     //### PERFORM CLEANSING OF PROXI PHASE ###//
#ifndef Phase_from_Length     
     cout << "startpoint= " << startpoint << " endpoint= " << endpoint << "\n";
     copier(Data.phase, Data.stretchedPhase);
     iterateTransform(&Data, startpoint, endpoint, in, p);//, fouriertrans, transform); //fourier cleansing
#else
     copier(Data.phase, Data.stretchedPhase);
     Cleansing_for_length_phases(Data, in, p);
#endif

#endif //off intermediate phase
    //### PRINT OUT OF THE ERROR ###//
    Calc_derivative(&Data, Data.derive_PPhase, Data.phase, Data.proofGrid, a, b, c);
    Calc_derivative(&Data, Data.derive_CPhase, Data.stretchedPhase, Data.proofGrid, a, b, c);
    cout << "print out period error" << "\n";
    errorfile << p << " " <<  Data.Error[0] << " " << INTEGRATED_STANDART_DEVIATION(Data.phase, Data.anaphase2, Data.proofGrid, Data, Nint2) << " " << INTEGRATED_STANDART_DEVIATION(Data.stretchedPhase, Data.anaphase2, Data.proofGrid, Data, Nint2) << " " << anaErr << " " << INTEGRATED_STANDART_DEVIATION(Data.modulation,Data.helpdata , Data.proofGrid, Data, Nint2) << " " << INTEGRATED_STANDART_DEVIATION(Data.derive_PPhase, Data.derive_APhase, Data.proofGrid, Data, Nint2) << " " << INTEGRATED_STANDART_DEVIATION(Data.derive_CPhase, Data.derive_APhase, Data.proofGrid, Data, Nint2) << " " << N_derive << " " << Data.Error[1] ;
    //### CALCULATE ALL PERIODICITY ERRORS ###//
    LengthOptimizer(Data, L, Nint2);
    errorfile << " " << Data.Error[0] << " " << Data.Error[1]; //11,12 periodicity error based on length
    L=2.0*pi;
    ERROR1(Data.data, Data.phase, Nint2, Data, L);
    errorfile << " " << Data.Error[0] << " " << Data.Error[1]; //13,14 periodicity error based on protophase 
    ERROR2(Data.data, Data.phase, Nint2, Data, L);
    errorfile << " " << Data.Error[0] << " " << Data.Error[1]; //15,16 normalized by variance
    ERROR1(Data.data, Data.stretchedPhase, Nint2, Data, L);
    errorfile << " " << Data.Error[0] << " " << Data.Error[1]; //17, 18 periodicity error based on cleansed protophase  
    ERROR2(Data.data, Data.stretchedPhase, Nint2, Data, L);
    errorfile << " " << Data.Error[0] << " " << Data.Error[1]; //19,20 normalized by variance
    errorfile << " " << anaVaraiance_data << " " << anaNorm_data << "\n"; //21,22 amplitude modulation level output also normalization integral if true phase is used
    //NOTE: Here the periodicity error of the former steps is printed but the
    //      recent step of the phase differences! 
#ifdef measuretime
    t1=clock();
    timesfile << "Phasecalc + Error calculation + Cleansing"<< p << " " << (DOUB)(t1-t2)/CLOCKS_PER_SEC << "\n";
#endif
   
    //### RECONSTRUCTION OF AMPLITUDE ###//
#ifdef AmplitudeReconstruction
    ConstructAmplitude(Data,L,in,Nmodes);// constructs Hilbert amplitude TODO: change here input parameter Nmodes!
#endif
    
    cout << "calc modulations of cleansed and uncleasned proxi phases \n";
    Calc_Modulation(Data.mod_proxip, Data.phase, Data.proofGrid, Data.maxindex[2], Data.maxindex[Data.maxindex.size()-3]);
    Calc_Modulation(Data.mod_Cproxip, Data.stretchedPhase, Data.proofGrid, Data.maxindex[2], Data.maxindex[Data.maxindex.size()-3]);
    //SawitzkyGolayFilterQ(Data.stretchedPhase, Data.proofGrid, a, b, c, 0);
    //### WRITE OUT DATA ###//
#ifdef write_one_intermediate_data
    if(p==Stepprint)
     {
#endif
#ifndef no_output
    dataprint(&Data, p, in, Nint2, n, path_to_folder);
#endif
#ifdef write_one_intermediate_data
     }
#endif
    
#ifdef measuretime
    t2=clock();
    timesfile << "DataPrint"<< p << " " << (DOUB)(t2-t1)/CLOCKS_PER_SEC << "\n";
#endif
   
    //### COPY PHASE OR LENGTH INTO GRID ###//
#ifdef Phase_from_Length
#ifdef case5
    copier(Data.phase, Data.Grid); //copy normalized phase to grid -> case 5
#endif
#ifdef case1
    copier(Data.length, Data.Grid); // copy length to grid TODO: One has to use the normlized length not length!
                                    // case 1-> normalization is passive (active but not needed), cleansing is passive
#endif
#ifdef case6
    copier(Data.stretchedPhase, Data.Grid); //case 6 -> active normalization, active cleansing
#endif
#else //not phase from length
#ifdef case2
    copier(Data.phase, Data.Grid);  // copy intermediate phase to grid 
                                    // case 2-> spline is active but cleansing is passive
#endif
#ifdef case3
    copier(Data.length, Data.Grid); //case 3-> interpolate maxima but use unnormalized length as phase
                                    // spline and cleansing remain passive    
#endif
#ifdef case4
    copier(Data.stretchedPhase, Data.Grid);  // copy intermediate stretched phase to grid
                                             // case 4-> spline and cleansing active
#endif
#endif
    
    // ### CALCULATE ERROR FUNCTION TO MINIMIZE (NOT MANDATORY)###//
#ifdef minimizer
    Minimizer_length(Data, p, in, Nint2);
#endif    
    // ### CALCULATE THE PHASE DISTRIBUTION ###// of phase (actual from bins and from fourier) stretched phase (actual)
#ifdef Phase_distribution
    Pdist(Data, startpoint, endpoint, in, p);
#endif

#ifdef measuretime
    t1=clock();
    timesfile << "Minimizer"<< p << " " << (DOUB)(t1-t2)/CLOCKS_PER_SEC << "\n";
#endif
   }//off loop p
  if(abord==0)
  {
  
  cout << "end of iteration process,  p=" << p << "\n";
  cout << "Step" << p << "+1: calculate phase, cleanse and print out results \n";

  //### CALLCULATE PHASE ###//
#ifndef Phase_from_Length
  phaseCalculator5(&Data, p, in);
#else
  //### FIND AVERAGE PERIOD LENGTH ###//
  LengthOptimizer(Data,L, Nint2);
  //### CALCULATE PHASE FROM LENGTH ###//
  phaseCalculator_Length(&Data, L, p, in);//calc phase from minimum length
#endif
  
  //### CLEANSE THE OBTAINED PHASE ###//
#ifndef Phase_from_Length     
  cout << "startpoint= " << startpoint << " endpoint= " << endpoint << "\n";
  copier(Data.phase, Data.stretchedPhase);
  iterateTransform(&Data, startpoint, endpoint, in, p);//, fouriertrans, transform);
#else   
  copier(Data.phase, Data.stretchedPhase);
  Cleansing_for_length_phases(Data, in, p);
#endif

  //### RECONSTRUCT AMPLITUDE ###//
  //TODO
#ifndef no_output
  dataprint(&Data, p, in, Nint2, n, path_to_folder);        //print out last Hilbert 
#endif
  cout << "final data print finished. \n";
  }
 errorfile.close();
 
 //### EXTRACT PARAMETRIC INFORMATION FOR THE PHASE MODULATION ###//
 
#ifdef parametric_phase
 //TODO: A METHOD TO EXAMINE THE MODULATIONS OF PHASES
 std::stringstream deb5;   //def file names
 deb5 << "Phase_Modulation_modes" << in << ".dat";
 std::string name2 = deb5.str();
 ofstream fouriertrans(name2,ios::out);
 fouriertrans.setf(ios::scientific);fouriertrans.precision(6);
 
 DOUB period(0.0);
 period  =Data.proofGrid[endpoint];
 period -=Data.proofGrid[startpoint];
 DOUB divider(2.0*pi/(period));
 
 for(int k=0; k<Data.data.size(); k++)
  { 
   Data.helpdata[k] = Data.phase[k] - Data.proofGrid[k];
  }
 vector<DOUB> evenpartTrue(Data.oddpart.size());
 vector<DOUB> oddpartTrue(Data.oddpart.size());
 
 FourierTrasform3(Data.proofGrid, Data.evenpart, Data.oddpart, Data.helpdata, Data.proofGrid, startpoint, endpoint, fouriertrans, divider);//calculate fourier modes for phase
 
 FourierTrasform3(Data.proofGrid, evenpartTrue, oddpartTrue, Data.anaphase2, Data.proofGrid, startpoint, endpoint, fouriertrans, divider);//calculate fourier modes for asymptotic phase
  
 for(int m=0; m<evenpartTrue.size();m++)
  { 
   fouriertrans << m << " " << Data.evenpart[m] << " " << Data.oddpart[m] << " " << evenpartTrue[m] << " " << oddpartTrue[m] << " " << "\n";
  }
 fouriertrans.close();
 
#endif 
if(abord==1){cout << "abord" << "\n";}
 
#ifdef measuretime
 timesfile << "Process time " << (DOUB)(clock() - starttime)/CLOCKS_PER_SEC << "\n";
 timesfile.close();
#endif
 
 return 0;  
}
