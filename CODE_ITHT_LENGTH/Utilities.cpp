// ###    DEF. OF UTILIT FUNCTS USED SEVERAL TIMES    ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "../HEADERFILES_ITHT_LENGTH/Main.h"

DOUB L2(DOUB &y1, DOUB &y2, DOUB &y3, DOUB &t1, DOUB &t2, DOUB &t3, DOUB &t)
 {
   return (t-t2)*(t-t3)/((t1-t2)*(t1-t3))*y1 + (t-t1)*(t-t3)/((t2-t1)*(t2-t3))*y2 + (t-t1)*(t-t2)/((t3-t1)*(t3-t2))*y3;
 }
 
DOUB findmax(vector<DOUB> &data, int &Nint2)//, const int&Nint) TODO: desing of code!!
 {
  DOUB m(-10.0);
  for(int k=Nint2; k<data.size()-Nint2; k++)
   {
    if(data[k]>m)
     {
      m=data[k];
     }
   }
  return m;
 }
  
 DOUB MeanCalc(Vectordata *Data)
  {
   DOUB mean(0.0);
   for(int k=Data->maxindex[1]; k<Data->maxindex[Data->maxindex.size()-2];k++)//calculate mean in full periods omit the boundaries, before k=0; k<Data->data.size()-1
    {
     mean += (Data->data[k]+Data->data[k+1])*0.5*(Data->Grid[k+1]-Data->Grid[k]);
    }
   return mean /=(Data->Grid[Data->maxindex[Data->maxindex.size()-2]]-Data->Grid[Data->maxindex[1]]);//Data->Grid.size()-1]-Data->Grid[0]);
  }
  
 DOUB MeanCalc_Length(Vectordata *Data)
  {
   DOUB mean(0.0);
   int start(Data->maxindex[1]), end(Data->maxindex[Data->maxindex.size()-2]);//start and end of integration
   
#ifdef Phase_from_Length
   start +=1;
   //NOTE: Handle the mismatch of index in time series and interpolated points (if no interpolated points are added)
   //      start here, not at T, X is the interpolated point that is not added to data
   //    T v
   //|****X***********************************************************X*****|
   
   if(Data->newtimes[1]<Data->timeGrid[Data->maxindex[1]])
    {
     start = Data->maxindex[1];
    }
    
   if(Data->newtimes[Data->newtimes.size()-2]<Data->timeGrid[Data->maxindex[Data->maxindex.size()-2]])
    {
     end=Data->maxindex[Data->maxindex.size()-2]-1;
    }
   mean = (Data->newdata[1]+Data->data[start])*0.5*(Data->Grid[start]-L2(Data->Grid[start-1], Data->Grid[start], Data->Grid[start+1], Data->timeGrid[start-1], Data->timeGrid[start], Data->timeGrid[start+1], Data->newtimes[1]));//first incomplete interval, length value is interpolated with second order lagrange polynomial
#endif

   for(int k=start; k<end;k++)//calculate mean in full periods omit the boundaries, before k=0; k<Data->data.size()-1
    {
     mean += (Data->data[k]+Data->data[k+1])*0.5*(Data->Grid[k+1]-Data->Grid[k]);
    }

#ifdef Phase_from_Length
   mean += (Data->data[end]+Data->newdata[Data->maxindex.size()-2])*0.5*(L2(Data->Grid[end-1], Data->Grid[end], Data->Grid[end+1], Data->timeGrid[end-1], Data->timeGrid[end], Data->timeGrid[end+1], Data->newtimes[Data->maxindex.size()-2])-Data->Grid[end]);//last incomplete interval, length value is interpolated with second order lagrange polynomial
#endif
   return mean /=(Data->Grid[Data->maxindex[Data->maxindex.size()-2]]-Data->Grid[Data->maxindex[1]]);//Data->Grid.size()-1]-Data->Grid[0]);
  }
  
 DOUB cubicSpline(DOUB L, DOUB Li, DOUB Lj, DOUB phasei, DOUB phasej, DOUB phase2i, DOUB phase2j)
  {
   DOUB A = (Lj-L)/(Lj-Li);
   DOUB B = 1.0 - A;
   DOUB C = (1.0/6.0)*(A*A*A-A)*(Lj-Li)*(Lj-Li);
   DOUB D = (1.0/6.0)*(B*B*B-B)*(Lj-Li)*(Lj-Li);
   return A*phasei + B*phasej + C*phase2i+D*phase2j;
  }
  
  void Structresize(Vectordata &Data, int &M)
  {
   Data.hilberttrafo.resize(Data.data.size());        //Hilberttrafo.cpp
   Data.htrafoVary.resize(Data.data.size());          //Hilberttrafo.cpp/Main.cpp/Amplitude.cpp
   Data.length.resize(Data.data.size());              //Main.cpp
   Data.phase.resize(Data.data.size());               //Main.cpp, Phasecalculation.cpp
   Data.periodphase.resize(Data.newtimes.size()+2);   //Phasecalculation.cpp
   Data.periodphase2.resize(Data.newtimes.size()+2);  //same
   Data.periodlength.resize(Data.newtimes.size()+2);  //same
   Data.proofGrid.resize(Data.data.size());           //Main.cpp
   Data.stretchedPhase.resize(Data.data.size());      //Main.cpp, Cleansing.cpp
   Data.helpdata.resize(Data.data.size());            //Cleansing.cpp
   Data.evenpart.resize(M);                           //Preprocessing.cpp
   Data.oddpart.resize(M);                            //oddpart.cpp
   //Data.anaphase.resize(Data.data.size());            //Preprocessing.cpp (dataGeneration)
   Data.Error.resize(2);                              //Main.cpp
   Data.modulation.resize(Data.data.size());          //main.cpp
   Data.envelope.resize(Data.data.size());            //Amplitude.cpp
   Data.Varydata.resize(Data.data.size());            //Amplitude.cpp
   Data.data3.resize(Data.data.size());               //Amplitude.cpp
   Data.GridAmp.resize(Data.data.size());             //Amplitude.cpp/Main.cpp/Hilberttrafo.cpp
   Data.mod_proxip.resize(Data.data.size());          //Cleansing.cpp/Utilities.cpp-> data print
   Data.mod_Cproxip.resize(Data.data.size());      //Cleansing.cpp/Utilities.cpp-> data print
   Data.derive_APhase.resize(Data.data.size());      //Cleansing.cpp/Utilities.cpp-> data print
   Data.derive_PPhase.resize(Data.data.size());      //Cleansing.cpp/Utilities.cpp-> data print
   Data.derive_CPhase.resize(Data.data.size());      //Cleansing.cpp/Utilities.cpp-> data print
  }
  
 void copier(vector<DOUB> vec, vector<DOUB> &vec2)
  {
   if(vec.size()!=vec2.size()) 
    {
     cout << "ERROR: size of vectors is different. To copy:" << vec.size() << " into " << vec2.size() << "\n";
    } 
   for(int k=0;k<vec.size();k++)
    {
     vec2[k] = vec[k];
    }
  }
 
 void dataprint(Vectordata *Data, int p, DOUB &in, int &Nint2, const int &n, string path_to_folder)
  {
   std::stringstream stream1;   //def file names
   stream1 << path_to_folder << "HilbertIt" << p+1 << "."<< in  << ".dat";
   std::string name1 = stream1.str();
   ofstream fout2(name1,ios::out);
   fout2.setf(ios::scientific);fout2.precision(15);
   cout << "Data print \n";
   for(int m=0; m<Data->data.size(); m++)
    {
#ifdef modulous
     if(m%n==0)
#endif
      {
       fout2 << Data->proofGrid[m] << " " << Data->originalData[m] << " " << Data->hilberttrafo[m] << " " << Data->phase[m] << " " << Data->stretchedPhase[m] << " " << Data->anaphase2[m] << " " << Data->length[m] << " " << Data->data[m] <<  " " << Data->envelope[m] << " " << Data->modulation[m] << " " << Data->mod_proxip[m] << " " << Data->mod_Cproxip[m] << " " << Data->derive_APhase[m] << " " << Data->derive_PPhase[m] << " " << Data->derive_CPhase[m]  << "\n";
      }
    }//off for int m=0 (main loop)
   cout << "Printed to file|time|raw data x|H[x]|proxi phase|cleansed proxi phase|true phase|length|normalized data|amplitude|true modulation|proxi phase modulation|cleansed proxi phase modulation|true IF| proxi IF|cleansed proxi IF| \n";
  }
 
void ERROR1(vector<DOUB> &data, vector<DOUB> &grid, int &Nint2, Vectordata &Data, DOUB &L)//Vectordata &Data, int &Nint2)//, const int &Nint)
 {
   Data.Error[0]=0.0;
   Data.Error[1]=0.0;
   int i(0);DOUB intTime(0.0);DOUB diff(0.0);DOUB del(0.0);
   int start(0), end(0);
   for(int k=0; k<Data.maxindex.size(); k++)//search for beginning of first full period in inner 80% of data
    {
     if(Data.maxindex[k]>=Nint2)
      {
       start=Data.maxindex[k];
       break;
      }
    }
   for(int k=0; k<Data.maxindex.size(); k++)//search for end of last full period in innner 80% of data
    {
     if(Data.maxindex[k]>data.size()-Nint2)
      {
       end=Data.maxindex[k-1];
       break;
      }
    }
   for(int k=start; k<end; k++)//main loop for error summation
    {
      intTime=grid[k]+L;//2.0*pi;
      for(int m=0;m<data.size();m++)//loop to find interpolation points
       { 
        if(grid[k+m]>=intTime)
         {
          i=m-1; 
          break;
         }
       }
      diff = (data[k] - L2(data[k+i-1],data[k+i],data[k+i+1],grid[k+i-1],grid[k+i],grid[k+i+1],intTime));
      del=(grid[k+1]-grid[k]);
      Data.Error[0] += diff*diff*del;
      Data.Error[1] += data[k]*data[k]*del;
    }
   Data.Error[0] /=Data.Error[1];
 }

void ERROR2(vector<DOUB> &data, vector<DOUB> &grid, int &Nint2, Vectordata &Data, DOUB &L)//Vectordata &Data, int &Nint2)//, const int &Nint)
 {
   Data.Error[0]=0.0;
   Data.Error[1]=0.0;
   int i(0);DOUB intTime(0.0);DOUB diff(0.0);DOUB del(0.0);
   int start(0), end(0);
   for(int k=0; k<Data.maxindex.size(); k++)//search for beginning of first full period in inner 80% of data
    {
     if(Data.maxindex[k]>=Nint2)
      {
       start=Data.maxindex[k];
       break;
      }
    }
   for(int k=0; k<Data.maxindex.size(); k++)//search for end of last full period in innner 80% of data
    {
     if(Data.maxindex[k]>data.size()-Nint2)
      {
       end=Data.maxindex[k-1];
       break;
      }
    }
   DOUB Md(MeanCalc2(data, grid, start, end)); //calculate mean of data
   for(int k=start; k<end; k++)//main loop for error summation
    {
      intTime=grid[k]+L;//2.0*pi;
      for(int m=0;m<data.size();m++)//loop to find interpolation points
       {
        if(grid[k+m]>=intTime)
         {
          i=m-1;
          break;
         }
       }
      diff = (data[k] - L2(data[k+i-1],data[k+i],data[k+i+1],grid[k+i-1],grid[k+i],grid[k+i+1],intTime));
      del=(grid[k+1]-grid[k]);
      Data.Error[0] += diff*diff*del;
      Data.Error[1] += (data[k]-Md)*(data[k]-Md)*del;
    }
   Data.Error[0] /=Data.Error[1];
 }

void mysort(vector<DOUB> &x, vector<DOUB> &y)
{
 vector<DOUB> sort_x(x);
 vector<DOUB> sort_y(y);
 int swapped(0), c(0);
 while(swapped==0)
  {
   swapped = 1;
   for(int i=0;i<x.size()-1;i++)
    {
     if(x[i] > x[i + 1])
      {
       //Swap the elements
       x[i]=sort_x[i+1];
       x[i+1]=sort_x[i];
       y[i]=sort_y[i+1];
       y[i+1]=sort_y[i];

       //copy the elements
       sort_x[i]=x[i];
       sort_x[i+1]=x[i+1];
       sort_y[i]=y[i];
       sort_y[i+1]=y[i+1];

       // Set the flag to True so we'll loop again
       swapped = 0;
      }
    }
   c+=1;
   cout << "swapping" << c << "\n";
   copier(sort_x, x);
   copier(sort_y, y);
  }
}

void linearFit(DOUB &m, DOUB &n, vector<DOUB> &data, vector<DOUB> &timeGrid, int &start, int &end, DOUB &stepadjust)
 {
  DOUB S(0.0), Sx(0.0), Sy(0.0), Sxx(0.0), Sxy(0.0);
  for(int k=start; k<=end; k++)
   {
    S   += 1.0/(DOUB(end-start));
    Sx  += timeGrid[k]/(DOUB(end-start));
    Sy  += data[k]/(DOUB(end-start));
    Sxx += timeGrid[k]*timeGrid[k]/(DOUB(end-start));
    Sxy += timeGrid[k]*data[k]/(DOUB(end-start));
   }
  m = (S*Sxy - Sx*Sy)/(S*Sxx - Sx*Sx);// slope
  n = (Sxx*Sy - Sx*Sxy)/(S*Sxx - Sx*Sx);// abs val
 }

void Calc_Modulation(vector<DOUB> &modulation, vector<DOUB> &phase, vector<DOUB> &timeGrid, int start, int end)
 {
  cout << "estimeate modulation \n";
  DOUB omeg(0.0), modulmean(0.0);//zero proxy of average growth, at maxpoints
  DOUB a(0.0);
  linearFit(omeg, modulmean, phase, timeGrid, start, end, a);//fit linear function parameters to phase
  for(int m=0;m<phase.size();m++)//calculate first proxy of modulation
   {
    a= phase[m];
    modulation[m] = a-omeg*timeGrid[m]-modulmean;
   }
 }

DOUB INTEGRATED_STANDART_DEVIATION(vector<DOUB> &data1, vector<DOUB> &data2, vector<DOUB> &Grid, Vectordata &Data, int &Nint2)
 {
   DOUB del(0.0), del2(0.0), del3(0.0), mean(0.0), integral_sigma(0.0);
   int start(0), end(0);
   for(int k=0; k<Data.maxindex.size(); k++)//search for beginning of first full period in inner 80% of data
    {
     if(Data.maxindex[k]>=Nint2)
      {
       start=Data.maxindex[k];
       break;
      }
    }
   for(int k=0; k<Data.maxindex.size(); k++)//search for end of last full period in innner 80% of data
    {
     if(Data.maxindex[k]>data1.size()-Nint2)
      {
       end=Data.maxindex[k-1];
       break;
      }
    }
   cout << "ISD: start= " << start << " end= " << end << "\n";
   //calculate mean
   del=0.0; del2=0.0; del3=0.0;
   for(int k=start; k<end; k++)
    {
     del=0.0; del2=0.0; del3=0.0;
     del  += Grid[k+1]; 
     del  -= Grid[k];
     del2 += data1[k+1]; 
     del2 -= data2[k+1];
     del3 += data1[k];
     del3 -= data2[k];
     mean += (del2+del3)*del;
    }
   del=0.0;
   del += Grid[end];
   del -= Grid[start];
   mean /= (2.0*del);
   del=0.0; del2=0.0; del3=0.0;
   for(int k=start; k<end; k++)//main loop for error summation
    {
     del=0.0; del2=0.0; del3=0.0;
     del  += Grid[k+1];
     del  -= Grid[k];
     del2 += data1[k+1];
     del2 -= mean;
     del2 -= data2[k+1];
     del3 += data1[k];
     del3 -= mean;
     del3 -= data2[k];
     integral_sigma += (del2*del2+del3*del3)*del;
    }
   del=0.0;
   del += Grid[end];
   del -= Grid[start];
   return integral_sigma/(2.0*del);
 }
  
 void Minimizer_length(Vectordata &Data, int p, DOUB &in, int &Nint2)
  {
   std::stringstream minerr; minerr << "TransError_Minimzer" << p+1 << "." << in << ".dat";
   std::string name01 = minerr.str(); ofstream errorfileL(name01,ios::out);
   errorfileL.setf(ios::scientific); errorfileL.precision(15);

   DOUB errold(0.0), Len1(0.0), Len4(0.0), Len3(Data.length[Data.maxindex[21]]), Len2(Data.length[Data.maxindex[20]]); 
   int maxcount(0);
   for(int k=1; k<Data.length.size(); k++)
    {
     ERROR1(Data.data, Data.length, Nint2, Data, Data.length[k]);
     if((Data.Error[0]<=errold) & (Len1==0.0))
      {
       Len1=Data.length[k];
       break;
      }
     errold=Data.Error[0];
    }   
   Len4=Len1+(Len3-Len2);
    
   DOUB dL(Len1*0.005), L(Len1);
   L=Len1;
   while(L<Len4)
    {
     ERROR1(Data.data, Data.length, Nint2, Data, L);
     errorfileL << L << " " << Data.Error[0] << "\n";
     if(0.1*Data.Error[0]>0.005)//step controlle for Length minimization, avoid to small steps
      {         
       L +=0.1*Data.Error[0];
      }
     else
      {
       L += 0.001;
      }
    }
   errorfileL.close();
  }
 
void preparation(Vectordata &Data, int &M, int &startpoint, int &endpoint, DOUB &r, int &Nint2)//, std::stringstream deb2)
 {
  startpoint=Data.maxindex[5];//before 0
  endpoint=Data.maxindex[Data.maxindex.size()-5];//before -1
  
  Structresize(Data,M);
  
  copier(Data.Grid, Data.proofGrid);    //for main algorithm
  
#ifdef frequencyUse
  int NN(1), Nsim(1);
  vector<DOUB> y(NN);
  vector<DOUB> dydx(NN);
  OMEG(y,dydx,NN,Data.proofGrid[0]);
  DOUB ddt(0.0), t(0.0); //set here 0 and then later variable size according to interpolated time points, also!: cast t again with 0.0, you can't use Data.proofGrid since it will be changed by rk4!!!
#endif
  
  for(int k=0;k<Data.data.size();k++)
   {
    Data.helpdata[k] = 1.0;//for cleansing 
    Data.envelope[k] = 1.0;//first approx. of amplitude ->to Amplitude.cpp
   }
#ifdef debugging
  cout << "count of Periods= " << count << "\n";   
#endif
 }

void Calc_derivative(Vectordata *Data, vector<DOUB> &derive_Phase, vector<DOUB> &data, vector<DOUB> &proofGrid, int a, int b, int c)
 {
  vector<DOUB> smoothdata(data.size());
  // deg rep   point derive (j,n)
  //int a(12), b(4), c(25), d(1);//d=0 -> phase smooth, d=1 -> phase derivative smooth;
  int d(1);
  copier(data, smoothdata);
  SawitzkyGolayFilterQ(smoothdata, proofGrid, a, b, c, d);
  copier(smoothdata,derive_Phase);
 }
 
DOUB MeanCalc2(vector<DOUB> &data, vector<DOUB> &time, int &start, int &end)
 {
  DOUB M(0.0);
  for(int k=start+1;k<end-1;k++)
   {
    M +=(data[k+1]+data[k])*(time[k+1]-time[k]);
   }
  return M*0.5/(time[end-1]-time[start+1]);
 }
 



