//### IMPLEMENTATION OF SPLINE PHASE CALCULATION                   ###//
//###                                                              ###//
//### AUTHOR: ERIK GENGEL, University of Potsdam                   ###//
//###                                                              ###//
#include "../HEADERFILES_ITHT_LENGTH/Main.h"

#ifndef length_calc_quadratic
void lengthCalculator(Vectordata *Data)
 {
  cout << "LengthCalculator linear interpolations: calc length \n";
  //### CALCULATE LENGTH OF THE TRAJECTORY ###//    
  Data->length[0] = 0.0;
  //cout << "length[0]=" << Data->length[0] << "\n";
  //Data->GridAmp[0] = 0.0;
  
  for(int k=1;k<Data->data.size();k++)
   {    
    Data->length[k] = Data->length[k-1]+sqrtl((Data->data[k] - Data->data[k-1])*(Data->data[k] - Data->data[k-1]) + (Data->hilberttrafo[k]-Data->hilberttrafo[k-1])*(Data->hilberttrafo[k]-Data->hilberttrafo[k-1]));
    //cout << "length[" << k << "]=" << Data->length[k] << "\n";
   }
 }
 
#else
void lengthCalculator_quadratic(Vectordata *Data)
 {  
  cout << "LengthCalculator quadratic interpolations: calc length \n";
  DOUB a(0.0), b(0.0), c(0.0), d(0.0);                     //Quadratic interpoaltion params
  DOUB alpa(0.0), beta(0.0), A(0.0), B(0.0), C(0.0), D(0.0), E(0.0), F(0.0), Len(0.0); //Quadratic length params
  Data->length[0] = 0.0;

  for(int k=1;k<Data->data.size()-1;k++)
   {
    Len=Data->length[k-1];
    Data->length[k]=0;
    
    F=Data->proofGrid[k-1]-Data->proofGrid[k];                                   //|
    a=((Data->data[k+1]-Data->data[k])*(Data->proofGrid[k-1]-Data->proofGrid[k])*(Data->proofGrid[k-1]-Data->proofGrid[k]) - (Data->data[k-1]-Data->data[k])*(Data->proofGrid[k+1]-Data->proofGrid[k])*(Data->proofGrid[k+1]-Data->proofGrid[k]))/((Data->proofGrid[k-1]-Data->proofGrid[k+1])*(Data->proofGrid[k+1]-Data->proofGrid[k])*(Data->proofGrid[k-1]-Data->proofGrid[k]));   
    b= ((Data->data[k-1]-Data->data[k])*(Data->proofGrid[k+1]-Data->proofGrid[k])-(Data->data[k+1]-Data->data[k])*(Data->proofGrid[k-1]-Data->proofGrid[k]))/((Data->proofGrid[k-1]-Data->proofGrid[k+1])*(Data->proofGrid[k+1]-Data->proofGrid[k])*(Data->proofGrid[k-1]-Data->proofGrid[k]));  
    c= ((Data->hilberttrafo[k+1]-Data->hilberttrafo[k])*(Data->proofGrid[k-1]-Data->proofGrid[k])*(Data->proofGrid[k-1]-Data->proofGrid[k]) - (Data->hilberttrafo[k-1]-Data->hilberttrafo[k])*(Data->proofGrid[k+1]-Data->proofGrid[k])*(Data->proofGrid[k+1]-Data->proofGrid[k]))/((Data->proofGrid[k-1]-Data->proofGrid[k+1])*(Data->proofGrid[k+1]-Data->proofGrid[k])*(Data->proofGrid[k-1]-Data->proofGrid[k]));  
    d= ((Data->hilberttrafo[k-1]-Data->hilberttrafo[k])*(Data->proofGrid[k+1]-Data->proofGrid[k])-(Data->hilberttrafo[k+1]-Data->hilberttrafo[k])*(Data->proofGrid[k-1]-Data->proofGrid[k]))/((Data->proofGrid[k-1]-Data->proofGrid[k+1])*(Data->proofGrid[k+1]-Data->proofGrid[k])*(Data->proofGrid[k-1]-Data->proofGrid[k]));  
    alpa=4.0*(a*b + c*d)/(a*a+c*c);                                              //|>>> interpolation parameters
    beta=4.0*(b*b+d*d)/(a*a+c*c);                                                //|    up to fourt order
    A=alpa*0.5;                                                                  //|
    B=beta*0.5-alpa*alpa*0.125;                                                  //|
    C=alpa*alpa*alpa/(16.0)-alpa*beta*0.25;                                      //|
    D=(3.0/16.0)*alpa*alpa*beta - (5.0/128.0)*alpa*alpa*alpa*alpa-beta*beta*0.125;  //|
    //-----------------------------------------------------------------------------|
   // cout << "a= " << a << " b= " << b << " c= " << c << " d= " << d << " A= " << A << " B= " << B << " C= " << C << " D= " << D << " E= " << E << " F= " << F << " alpha= " << alpa << " beta= " << beta << "\n";
    Data->length[k] = Len - sqrt(a*a+c*c)*(F + 0.5*A*F*F + (B/3.0)*F*F*F + C*0.25*F*F*F*F + 0.2*D*F*F*F*F*F);   
   }
  //### LENGTH OF VERY LAST POINT ###//
  //reuse the interpolated polynomial and insert an other upper integration boundary
  
  F=Data->proofGrid[Data->data.size()-2]-Data->proofGrid[Data->data.size()-1];                                   //|
  Len=Data->length[Data->data.size()-2];
  Data->length[Data->data.size()-1] = 0;
  Data->length[Data->data.size()-1] = Len - sqrt(a*a+c*c)*(F + 0.5*A*F*F + (B/3.0)*F*F*F + C*0.25*F*F*F*F + 0.2*D*F*F*F*F*F);
     
 }
#endif

 void phaseCalculator5(Vectordata *Data, int p, DOUB &in)
  { 
#ifdef debuggingPhase
    std::stringstream deb4;   //def file names
    deb4 << in << "Phase" << p+1 << ".dat";
    std::string name3 = deb4.str();
    ofstream foutdebug4(name3,ios::out);
    foutdebug4.setf(ios::scientific);foutdebug4.precision(15);
#endif
#ifndef arctanPhase
    
   cout << "phase: initial integers \n";

   //### CALCULATE INITIAL INTEGERS FOR LENGTH INTERVALS ###//
   int i(0), j(Data->maxindex[0]);
   DOUB shift(0.0);
   int initial(j);//remember for boundaries
   int End(Data->maxindex[Data->maxindex.size()-1]);

   int counter(0);   
#ifdef splinePhase
   cout << "phase: spline preparation \n";
   
    
    //### CALCULATE BOUNDARY PHASES AND DERIVATIVES USING THE TRIDIAGONAL ALGORITHM ###//
   
    Data->periodphase[0] = 2.0*pi*(Data->length[Data->maxindex[1]]-2.0*Data->length[Data->maxindex[0]])/(Data->length[Data->maxindex[1]]-Data->length[Data->maxindex[0]]); //here -2.0*start because first full period is used
  
    Data-> periodphase[1] = 2.0*pi;
    
    for(int k=2; k<=Data->maxindex.size();k++)
     {
      Data->periodphase[k] = Data->periodphase[k-1] + 2.0*pi;//(long double)(k)*2.0*pi; //first new time is not at theta=0 but at theta=2pi
     }
     
    Data->periodphase[Data->maxindex.size()+1]=Data->periodphase[Data->maxindex.size()] + 2.0*pi*(Data->length[Data->length.size()-1]-Data->length[Data->maxindex[Data->maxindex.size()-1]])/(Data->length[Data->maxindex[Data->maxindex.size()-1]]-Data->length[Data->maxindex[Data->maxindex.size()-2]]);//per.phase is 2 elemnts longer than D.d
    Data->periodlength[0]=0.0;
 
    //### SET PERIOD PHASES ###//
    for(int k=0; k<Data->phase.size(); k++)
     {
      if(k==Data->maxindex[counter])
       {
        Data->periodlength[counter+1] = Data->length[k];
        counter +=1;
       }
     }
   Data->periodlength[Data->maxindex.size()+1]=Data->length[Data->length.size()-1]; //for last point
   
   //### DEFINE DERIVATIVES AT FIRST AND LAST POINT ###//
   //NOTE: These two parameters are not used yet. Instead, a natural spline is used
   DOUB deriveBegin = 1.0;//2.0*pi/(Data->length[j] + Data->length[Data->data.size()-1] -Data->length[End]);
   DOUB deriveEnd = 1.0;//2.0*pi/(Data->length[Data->data.size()-1]+Data->length[initial]-Data->length[i]);
   
   //### CALCULATE DERIVATIVES IN LENGTH FOR PHASES ###//
   setPeriodPhase2(Data, deriveBegin, deriveEnd); 
  
   cout << "phase: calc phase from spline \n";
#endif
   //### CALCULATE THE PHASE FROM LENGTH ###//
   counter = 0;
   for(int k=0; k<Data->data.size();k++)
    {
     //### calculate new integers for length ###//
     if(k==Data->maxindex[counter])//if event takes place set new index for boundary
      {
       i=k;//old index
       j=Data->maxindex[counter+1];//new index
       shift+=1.0;//unwrap phase
       counter +=1;//index for max point vector
      }
#ifdef splinePhase
     //### here: calculate phase from spline ###//
     Data->phase[k] = cubicSpline(Data->length[k], Data->periodlength[counter], Data->periodlength[counter+1], Data->periodphase[counter], Data->periodphase[counter+1], Data->periodphase2[counter], Data->periodphase2[counter+1]);
#endif
#ifdef lengthPhase
     if(Data->proofGrid[k]<Data->newtimes[0])
      {
       //foutdebug4 << "initial=" << initial << " maxindex[0]= " << Data->maxindex[0] << "i= " << i << "j= " << j << "\n";
       Data->phase[k] = 2.0*pi*Data->length[k]/(Data->length[Data->maxindex[1]]-Data->length[initial]) + 2.0*pi*(Data->length[Data->maxindex[1]]-2.0*Data->length[initial])/(Data->length[Data->maxindex[1]]-Data->length[initial]);
      }
     if((Data->proofGrid[k]>=Data->newtimes[0]) & (Data->proofGrid[k]<Data->newtimes[Data->newtimes.size()-1]))
      {
       //foutdebug4 << "initial=" << initial << " maxindex[0]= " << Data->maxindex[0] << "i= " << i << "j= " << j << "\n";  
       Data->phase[k] = 2.0*pi*(Data->length[k]/Data->length[i]-1.0)/(Data->length[j]/Data->length[i]-1.0) + shift*2.0*pi; 
      }
     if(Data->proofGrid[k]>=Data->newtimes[Data->newtimes.size()-1])
      {
       //foutdebug4 << "initial=" << initial << " maxindex[0]= " << Data->maxindex[0] << "i= " << i << "j= " << j << "\n";   
       Data->phase[k] = 2.0*pi*(Data->length[k]-Data->length[i])/(Data->length[Data->maxindex[Data->maxindex.size()-1]]-Data->length[Data->maxindex[Data->maxindex.size()-2]]) + shift*2.0*pi;
      }
#endif
    }//off loop poins length spline
#endif
#ifdef arctanPhase
  DOUB shift1 = 0.0;
  DOUB oldphase(0.0), newphase(0.0);
  DOUB pha(0.0), amplitude_correction(0.0);
  for(int i=0; i<Data->data.size(); i++)
   {
    oldphase = newphase;
    pha = atan2(Data->hilberttrafo[i],Data->data[i]);//hilbertiterateold[i]);
    newphase = pha + amplitude_correction;
    if((newphase < 0.0) & (oldphase > 0.0) & Data->data[i] < 0.0)
     {
      shift1 +=1.0;
     }
    if((newphase > 0.0) & (oldphase < 0.0) & Data->data[i] < 0.0)
     {
      shift1 -=1.0;
     }
    Data->phase[i] = pha + shift1*2.0*pi;
    } 
#endif
   
#ifdef debuggingPhase
   for(int k=0; k<Data->phase.size();k++)
    {
     foutdebug4 << Data->proofGrid[k] << " " << Data->phase[k] << "\n";
    }
   foutdebug4.close();
#endif
 }

void LengthOptimizer(Vectordata &Data, DOUB &L, int &Nint2)
 {
  //Under knowledge of interpolated points the found minimum in the interval will be accurate 
  DOUB errold(0.0), Len1(0.0), Len4(0.0), Len3(Data.length[Data.maxindex[21]]), Len2(Data.length[Data.maxindex[20]]), err1(0.0), err2(0.0), err3(0.0), err4(0.0); 
  //NOTE: Here Len3 and Len2 are choosen in such a way that they are in the range for a good estimate of periodicity
  int count(0);
  for(int k=1; k<Data.length.size(); k++)
   {
    ERROR1(Data.data, Data.length, Nint2, Data, Data.length[k]);
    if((Data.Error[0]<=errold) & (Len1==0.0))
     {
      Len1=Data.length[k];
      break;
     }
    errold=Data.Error[0];
   }   
  Len4=Len1+(Len3-Len2);
  //NOTE:Main maximum should be inbetween the interval [L1,L4] (Len3-Len2) is an approximate period
  //     with one minimum of error in between
   
  cout << "LengthOptimizer: LengthRange: [L1,L2]= [" << Len1 << "," << Len4 << "]\n";
  
  Len2=(Len4-Len1)*0.49+Len1;//set middle length for search
  Len3=(Len4-Len1)*0.51+Len1;
  
  //### Calc Errors ###//
  ERROR1(Data.data, Data.length, Nint2, Data, Len1);
  err1=Data.Error[0];
  ERROR1(Data.data, Data.length, Nint2, Data, Len2);
  err2=Data.Error[0];
  ERROR1(Data.data, Data.length, Nint2, Data, Len3);
  err3=Data.Error[0];
  ERROR1(Data.data, Data.length, Nint2, Data, Len4);
  err4=Data.Error[0];
  
  //### Find optimal L ###//
  
  do
   {
    if(err2<err3)//chose left interval
     {
      Len4=Len3;
      Len3=(Len4-Len1)*0.55+Len1;
      Len2=(Len4-Len1)*0.45+Len1;
     }
    else//chose right interval
     {
      Len1=Len2;
      Len2=(Len4-Len1)*0.45+Len1;
      Len3=(Len4-Len1)*0.55+Len1;
     }
    ERROR1(Data.data, Data.length, Nint2, Data, Len1);
    err1=Data.Error[0];
    ERROR1(Data.data, Data.length, Nint2, Data, Len2);
    err2=Data.Error[0];
    ERROR1(Data.data, Data.length, Nint2, Data, Len3);
    err3=Data.Error[0];
    ERROR1(Data.data, Data.length, Nint2, Data, Len4);
    err4=Data.Error[0];
    cout << "L1= " << Len1 << " L2= " << Len2 << " L3= " << Len3 << " L4= " << Len4 << " err1= " << err1 << " err2= " << err2 << " err3= " << err3 << " err4= " << err4 << "\n";
    count +=1;
   }
  while(count<50);
  //NOTE: Some large number at which convergence is achieved 
  cout << "L1= " << Len1 << " L2= " << Len2 << " L3= " << Len3 << " L4= " << Len4 << " err1= " << err1 << " err2= " << err2 << " err3= " << err3 << " err4= " << err4 << "\n";
    
  L=(Len3+Len2)*0.5;
  cout << "optimal Length= " << L << "\n";  
 }
 
void phaseCalculator_Length(Vectordata *Data, DOUB &L, int p, DOUB &in)
 {
  cout << "Start Phase calculation by length \n";
  
  cout << "L = " << L << " Length from periods= " << Data->length[Data->maxindex[17]] - Data->length[Data->maxindex[16]] << " length to subtract= " <<  Data->length[Data->maxindex[0]] << "\n";
  
  DOUB phi0(0.0);
  int counter(0);
  phi0=2.0*pi*(L-Data->length[Data->maxindex[0]])/L;//first incomplete period
  for(int k=0;k<Data->data.size(); k++)
   {
    Data->phase[k] = phi0 + 2.0*pi*Data->length[k]/L;// + 2.0*pi*DOUB(counter);//unwraped phase
   }
 }

 void setPeriodPhase2(Vectordata *Data, DOUB &deriveBegin, DOUB &deriveEnd)
  {
    std::stringstream enve2;   //def file names
    enve2 <<  "enelope_spline"  << ".dat";
    std::string nameenve2 = enve2.str();
    ofstream enve22(nameenve2,ios::out);
    enve22.setf(ios::scientific);enve22.precision(15);
    
   int i(0),k(0);
   DOUB p, qn, sig, un;
   int n=Data->periodphase2.size();
   vector<DOUB> u(n-1);
   Data->periodphase2[0] = u[0] = 0.0;//natural spline
   for(i=1; i<n-1; i++)
    {
     sig=(Data->periodlength[i]-Data->periodlength[i-1])/(Data->periodlength[i+1]-Data->periodlength[i-1]);
     p=sig*Data->periodphase2[i-1]+2.0;
     Data->periodphase2[i]=(sig-1.0)/p;
     u[i]=(Data->periodphase[i+1]-Data->periodphase[i])/(Data->periodlength[i+1]-Data->periodlength[i])-(Data->periodphase[i]-Data->periodphase[i-1])/(Data->periodlength[i]-Data->periodlength[i-1]);
     u[i]=(6.0*u[i]/(Data->periodlength[i+1]-Data->periodlength[i-1]) - sig*u[i-1])/p;
    }
   qn=un=0.0;//natural spline
   Data->periodphase2[n-1]=(un-qn*u[n-2])/(qn*Data->periodphase2[n-2]+1.0);
   for(k=n-2;k>=0;k--)
    {
     Data->periodphase2[k]=Data->periodphase2[k]*Data->periodphase2[k+1]+u[k];
    }
   for(int k=0; k<Data->periodphase2.size();k++)
    {
     enve22 << Data->periodphase2[k] << " " << Data->periodlength[k] << " " << Data->periodphase[k] << "\n";
    }
   enve22.close();
  }
 
