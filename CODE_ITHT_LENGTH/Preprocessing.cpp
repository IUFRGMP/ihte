// ### DEFINITION OF PREPROCESSING FUNCTIONS          ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "../HEADERFILES_ITHT_LENGTH/Main.h"

#ifndef fromfile

void OMEG(vector<DOUB> y, vector<DOUB> &dydx, int N, DOUB &t)
 {
  DOUB x((t));
  if(cos(sqrt(2.0)*0.25*t)>0.0)
   {
    dydx[0] = 0.5;
   }
  else
   {
    dydx[0] = 1.2;
   }
 }

DOUB phaseFunction(DOUB &x, DOUB &r, DOUB &initValue, Vectordata &Data)
 {
#ifdef frequencyUse
  return initValue;// uses phase given from outside
#else
  initValue = 1.0 + Data.a*Data.f*cos(Data.f*x) - Data.b*Data.g*sin(Data.g*x);
  return x + Data.a*sin(Data.f*x) + Data.b*cos(Data.g*x);
#endif
}
 
void rk4(void (*derives)(vector<DOUB> y, vector<DOUB> &dydx, int N, DOUB &t), vector<DOUB> &y, int Nsim, DOUB &t, int N, DOUB dt){
 vector<DOUB> dydx1(N);
 vector<DOUB> dydx2(N);
 vector<DOUB> dydx3(N);
 vector<DOUB> dydx4(N);
 vector<DOUB> y1(N);
 vector<DOUB> y2(N);
 vector<DOUB> y3(N);

 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, dydx1,N,t);//k1
   for(int i=0; i<N; i++)
    {
     y1[i] = y[i] + 0.5*dt*dydx1[i];//for k2
    }
   t = t + 0.5*dt;
   (*derives)(y1,dydx2,N,t);//k2
    for(int i=0; i<N; i++)
    {
     y2[i] = y[i] + 0.5*dt*dydx2[i] ;//for k3
    }
   (*derives)(y2,dydx3,N,t);
   for(int i=0; i<N; i++)
    {
     y3[i] = y[i] + dt*dydx3[i];
    }
   t = t + 0.5*dt;
   (*derives)(y3,dydx4,N,t);
   for(int i=0; i<N; i++)
    {
     y[i] = y[i] + ((dt/6.0)*dydx1[i] + (dt/3.0)*dydx2[i] + (dt/3.0)*dydx3[i] + (dt/6.0)*dydx4[i]);
    }
  }
}

vector<DOUB> noiseGauss(DOUB &mean, DOUB &sigma)
{
vector<DOUB> rands(2);
DOUB u1,u2;
u1 = DOUB(rand()) * (1.0/(DOUB(RAND_MAX)));
u2 = DOUB(rand()) * (1.0/(DOUB(RAND_MAX)));
rands[0] = mean + sigma * sqrt(-2.0*log(u1)) * cos(2.0*pi*u2);
rands[1] = mean + sigma * sqrt(-2.0*log(u1)) * sin(2.0*pi*u2);
return rands;
}

 
#ifdef mono
DOUB Signal(DOUB &phase, DOUB &t, DOUB &amp, DOUB &omeg, DOUB &Gnoise)
 {
#ifdef Addnoise
  return cos(phase)+Gnoise;
#else
  return cos(phase)+amp*sin(sqrt(2.0)*phase);
#endif
 }
 
#else
DOUB Signal(DOUB &phase, DOUB &t, DOUB &amp, DOUB &omeg, DOUB &Gnoise)
  {
   DOUB s(0.0);
      for(int k=1;k<=Modes;k++)
          {
           s += (1.0/((DOUB)(k))*sin(phase*(2.0*((DOUB)(k))))+(1.0/((DOUB)(k)))*cos(phase*((DOUB)(k))));
          }
   s = s/((DOUB)(Modes));
   //return s;
#ifdef Addnoise
   return cos(phase) - 0.7*sin(2.0*phase)+cos(3.0*phase)+Gnoise; 
#else
   return 2.0*sin(phase) - cos(phase)*exp(2.0*sin(phase));
#endif
}
#endif

void dataGenerator1(DOUB (*Signal)(DOUB &phase, DOUB &t, DOUB &amp, DOUB &omeg, DOUB &Gnoise), DOUB (*phase)(DOUB &t, DOUB &r, DOUB &initValue, Vectordata &Data), Vectordata &Data, DOUB &t, DOUB &omeg, DOUB &amp, DOUB &noiseamplitude, DOUB &sigma, DOUB &r, DOUB &in, DOUB &initValue)
{ 
#ifdef debuggingDatagen
  std::stringstream deb0;   //def file names
  deb0 << "Datageneration." << in << ".dat";
  std::string name0 = deb0.str();
  ofstream foutdebug1(name0,ios::out);
  foutdebug1.setf(ios::scientific);foutdebug1.precision(15);
#endif
 DOUB Phi(0.0);
 
#ifdef frequencyUse
 int NN(1), Nsim(1);
 vector<DOUB> y(NN);
 vector<DOUB> dydx(NN);
 OMEG(y,dydx,NN,t);
 DOUB ddt(dt);//to change later in preparation function dt ist const DOUB ddt is DOUB
#endif
 
 DOUB Gnoise(0.0);
#ifdef Addnoise
 srand(time(NULL));
 vector<DOUB> GausianNoise(2);
#endif
 
 DOUB tt(0.0), fval(0.0);
 
 for(int i=0; i<Data.sl; i++)                //generate data
   {
#ifdef frequencyUse
    initValue=y[0];
    Data.frequency.push_back(dydx[0]);
#endif
#ifdef Addnoise//disturb signal with additive noise
    GausianNoise = noiseGauss(noiseamplitude,sigma);
    Gnoise=GausianNoise[0];//noiseamplitude*DOUB(rand())/(DOUB(RAND_MAX));
#endif
    initValue=0.0;
    Phi=(*phase)(t,r,initValue,Data);
    DOUB s = (*Signal)(Phi,t,amp,omeg,Gnoise);
    Data.data2.push_back(s);
    Data.timeGrid.push_back(t);
    Data.anaphase.push_back(Phi);
    Data.anafreq.push_back(initValue);
#ifdef frequencyUse
#ifdef anafreq_use
    Data.anfreq.push_back(dydx[0]);
#endif
    rk4(OMEG,y,Nsim,t,NN,ddt); //update phase
 //   OMEG(y,dydx,NN,t);         //update frequency independently
    tt = tt+Data.dt;
#else
    t=t+Data.dt;
#endif
   } 
#ifdef debuggingDatagen
 for(int k=0;k<Data.data2.size();k++)
  {
#ifdef frequencyUse
   foutdebug1 << Data.timeGrid[k] << " " << Data.data2[k] << " " << Data.frequency[k] << " " << tt << "\n";
#else
   foutdebug1 << Data.timeGrid[k] << " " << Data.data2[k] << "\n";
#endif

  }
  foutdebug1.close();
#endif
}

#else //if def from file
void extractor(Vectordata &Data , string &datafile)
 {
  cout << "reading a file" << "\n";
  string line;
  ifstream myfile (datafile);
  //### EXTRACT FILE CONTENT OF FORM data <SPACE> time \n to struct members ###//
  if(myfile.is_open())
   {
    DOUB a(0.0), b(0.0), c(0.0), d(0.0), e(0.0);
    while(getline(myfile,line))
     {
      std::stringstream ss(line);
      istringstream is(line);
      vector<DOUB> vecline;
      int jj=0;
      while(is >> a)
       {
    	if(jj==0)
	     {
          vecline.push_back(DOUB(a));
	  	 }
	    if(jj==Data.xcol-1)
    	 {
	      vecline.push_back(DOUB(a));
	     }
	    if(jj==Data.phasecol-1)
	     {
          vecline.push_back(DOUB(a));
         }
    	if(jj==Data.derivecol-1)
         {
          vecline.push_back(DOUB(a));
         }
    	jj +=1;
       }
      if((Data.derivecol>Data.xcol)&&(Data.derivecol>Data.phasecol))//derive col after data and phase
       {
        if(Data.xcol<Data.phasecol)
         {
          b=vecline[1];//x
          d=vecline[2];//phi
          c=vecline[3];//dphi
         }
        if(Data.xcol>Data.phasecol)//switch places here if data col comes after phase col!
         {
          b=vecline[2];
          d=vecline[1];
	      c=vecline[3];
         }
       }
      if((Data.derivecol<Data.xcol)&&(Data.derivecol<Data.phasecol))//derive col before data and phase
       {
        if(Data.xcol<Data.phasecol)
         {
          b=vecline[2];
          d=vecline[3];
          c=vecline[1];
         }
        if(Data.xcol>Data.phasecol)//switch places here if data col comes after phase col!
         {
          b=vecline[3];
          d=vecline[2];
          c=vecline[1];
         }
       }
      if((Data.derivecol<Data.xcol)&&(Data.derivecol>Data.phasecol)||(Data.derivecol>Data.xcol)&&(Data.derivecol<Data.phasecol))//derive col in between data and phase
       {
        if(Data.xcol<Data.phasecol)
         {
          b=vecline[1];
          d=vecline[3];
          c=vecline[2];
         }
        if(Data.xcol>Data.phasecol)//switch places here if data col comes after phase col!
         {
          b=vecline[3];
          d=vecline[1];
          c=vecline[2];
         }
       }
      Data.data2.push_back(b);//NOTE: b for the x component of the dynamics
      a=vecline[0];
      Data.timeGrid.push_back(a);
      Data.anaphase.push_back(d);//true phase 
      Data.anafreq.push_back(c);//true frequency of true phase from external calculations
     }
    myfile.close();
   } 
  cout << "######################### \n";
  
  DOUB timeofset(Data.timeGrid[0]);
  
  for(int k=0;k<Data.data2.size();k++)
   {
    Data.timeGrid[k] -= timeofset;
    cout << Data.data2[k] << " " << Data.timeGrid[k] << "\n";
   }   
 }
#endif

DOUB L5(DOUB &f0,DOUB &f1,DOUB &f2,DOUB &f3,DOUB &f4,DOUB &f5,DOUB &X0,DOUB &X1,DOUB &X2,DOUB &X3,DOUB &X4,DOUB &X5,DOUB &x)
 {
  DOUB l0(0.0), l1(0.0), l2(0.0), l3(0.0), l4(0.0), l5(0.0);
  l0=(x-X1)/(X0-X1)*(x-X2)/(X0-X2)*(x-X3)/(X0-X3)*(x-X4)/(X0-X4)*(x-X5)/(X0-X5);
  l1=(x-X0)/(X1-X0)*(x-X2)/(X1-X2)*(x-X3)/(X1-X3)*(x-X4)/(X1-X4)*(x-X5)/(X1-X5);
  l2=(x-X0)/(X2-X0)*(x-X1)/(X2-X1)*(x-X3)/(X2-X3)*(x-X4)/(X2-X4)*(x-X5)/(X2-X5);
  l3=(x-X0)/(X3-X0)*(x-X1)/(X3-X1)*(x-X2)/(X3-X2)*(x-X4)/(X3-X4)*(x-X5)/(X3-X5);
  l4=(x-X0)/(X4-X0)*(x-X1)/(X4-X1)*(x-X2)/(X4-X2)*(x-X3)/(X4-X3)*(x-X5)/(X4-X5);
  l5=(x-X0)/(X5-X0)*(x-X1)/(X5-X1)*(x-X2)/(X5-X2)*(x-X3)/(X5-X3)*(x-X4)/(X5-X4);
  return f0*l0+f1*l1+f2*l2+f3*l3+f4*l4+f5*l5;
 }
 
void LAGRANGE_MAX(DOUB &f0,DOUB &f1,DOUB &f2,DOUB &f3,DOUB &f4,DOUB &f5,DOUB &X0,DOUB &X1,DOUB &X2,DOUB &X3,DOUB &X4,DOUB &X5,DOUB &interTime, DOUB &dataval)//NOTE: This functin determines the maximum time and the maximum value of data based on a 6-point symetirc lagrange polynomial at the maximum point  
 { 
  DOUB x1(X1), x2(X2), x3(X3), x4(X4), xright(X5), P1(f1), P2(f2), P3(f3), P4(f4), l0(0.0), l1(0.0), l2(0.0), l3(0.0), l4(0.0), l5(0.0);
  DOUB x(0.0);//set starting point
  //start optimization:
  
  cout << "LAGRANGE_MAX: x1= " << x1 << " x2= " << x2 << " x3= " << x3 << " x4= " << x4 << " xright= " << xright << " f0= " << f0 << " f1= " << f1 << " f2= " << f2 << " f3= " << f3 << " f4= " << f4 << "\n";

  //### Find optimal L ###//for(int k=0;k<50;k++)
  int count(0);
  do
   {
    //cout << "L1= " << x1 << " L2= " << x2 << " L3= " << x3 << " L4= " << x4 << " err1= " << P1 << " err2= " << P2 << " err3= " << P3 << " err4= " << P4 << "\n";
    if(P2>P3)//chose left interval
     {
      x4=x3;
      x3=(x4-x1)*0.55+x1;
      x2=(x4-x1)*0.45+x1;
     }
    else//chose right interval
     {
      x1=x2;
      x2=(x4-x1)*0.45+x1;
      x3=(x4-x1)*0.55+x1;
     }
    x=x1;
    P1=L5(f0,f1,f2,f3,f4,f5,X0,X1,X2,X3,X4,X5,x);
     
    x=x2;
    P2=L5(f0,f1,f2,f3,f4,f5,X0,X1,X2,X3,X4,X5,x);
  
    x=x3;
    P3=L5(f0,f1,f2,f3,f4,f5,X0,X1,X2,X3,X4,X5,x);
    
    x=x4;
    P4=L5(f0,f1,f2,f3,f4,f5,X0,X1,X2,X3,X4,X5,x);
    
    count +=1;
   }
  while(count<50);
  //NOTE: Some large number at which convergence is achieved 
  //cout << "L1= " << x1 << " L2= " << x2 << " L3= " << x3 << " L4= " << x4 << " err1= " << P1 << " err2= " << P2 << " err3= " << P3 << " err4= " << P4 << "\n";
    
  //calculate final maximum value and maximum time
  x=(x3+x2)*0.5;
  interTime=x;
  dataval = L5(f0,f1,f2,f3,f4,f5,X0,X1,X2,X3,X4,X5,x);
  //cout << "maximum time= " << x << "\n";  

 }

void InitialDataCopy(Vectordata &Data, DOUB &in, int &Nint2, DOUB &thresholdext)
 {
  //NOTE: Generates from initial time grid and data (stored in Data.data2)->
  //      - Data.data -> copy of Data.data2         (used in the whole transform process)
  //      - Data.Grid -> copy of Data.timeGrid      (used and overwritten)
  //      - Data.OrigianData -> copy of Data-data2  (no use yet)
  //      - Data.maxindex                           (used for Cleansing -> determines full periods)
  //      - Data.newdata, Data.newtimes             (not used for lengthphase, kept for spline,length,atan phase)
  //      - Data.anaphase2                          (asymptotic phase for dynamical systems with (lengthphase) or
  //                                                 without interpolated points)
  //      - Data.anafreq2                           (stores the true raw data of true phse derivative from file with extra points)

#ifdef debuggingmaxinterpolator
  std::stringstream deb1;   //def file names
  deb1 << in << "maxinterpolation" << ".dat";
  std::string name1 = deb1.str();
  ofstream foutdebug2(name1,ios::out);
  foutdebug2.setf(ios::scientific);foutdebug2.precision(15);
#endif
  long double interTime(0.0), a(0.0), b(0.0);
  int counter(0), index(0);
  DOUB maxold(0.0);
  
  //### calculate mean of data, determine global maximum ###//
  
  DOUB maxi(findmax(Data.data2,Nint2)); //find global maximum  
  DOUB mean(0.0);DOUB meanit(1.0);DOUB del(0.0);
  while(abs(meanit)>=1e-9)//iterate mean of data for better approximation
   {
    for(int k=0; k<Data.data2.size()-1;k++)
     {
      del=(Data.timeGrid[k+1]-Data.timeGrid[k]);
      meanit += (Data.data2[k]+Data.data2[k+1])*0.5*del - mean*del;
     }
    meanit /=(Data.timeGrid[Data.timeGrid.size()-1]-Data.timeGrid[0]);
    mean+=meanit;
    cout << "InitialDataCopy: meanit= " << meanit << " mean= " << mean << "\n";
   }

   //### Main loop starts ###//
   int beneath(1), searchmaxind(0), exactmaxind(0);
   DOUB maxtimewindow(4.5), olddat(0.0);//search window for better maximum in the presence of noise, dummy
    for(int k=1;k<Data.data2.size()-1;k++)//iterate through points
     {
      if(Data.data2[k]<=mean)
       {
        beneath=1;
       }
      if(((Data.data2[k] - mean)>= thresholdext*(maxi-mean)) & (beneath == 1))//NOTE: threshold is external argument of programme
       {
        if((Data.data2[k-1]<Data.data2[k]) & (Data.data2[k]>Data.data2[k+1]))//NOTE: Point at index k is the maximum point
         {
          beneath=0;/*
          //calculate derivative parameters for L2 -> gives maximum time (interTime) and value of data
          a= 2.0*Data.data2[k-1]/((Data.timeGrid[k-1]-Data.timeGrid[k])*(Data.timeGrid[k-1]-Data.timeGrid[k+1])) + 2.0*Data.data2[k]/((Data.timeGrid[k]-Data.timeGrid[k-1])*(Data.timeGrid[k]-Data.timeGrid[k+1])) + 2.0*Data.data2[k+1]/((Data.timeGrid[k+1]-Data.timeGrid[k-1])*(Data.timeGrid[k+1]-Data.timeGrid[k]));
         
          b= -Data.data2[k-1]*(Data.timeGrid[k]+Data.timeGrid[k+1])/((Data.timeGrid[k-1]-Data.timeGrid[k])*(Data.timeGrid[k-1]-Data.timeGrid[k+1])) - Data.data2[k]*(Data.timeGrid[k-1]+Data.timeGrid[k+1])/((Data.timeGrid[k]-Data.timeGrid[k-1])*(Data.timeGrid[k]-Data.timeGrid[k+1])) -Data.data2[k+1]*(Data.timeGrid[k-1]+Data.timeGrid[k])/((Data.timeGrid[k+1]-Data.timeGrid[k-1])*(Data.timeGrid[k+1]-Data.timeGrid[k]));
            interTime=-b/a;
          //store max times, max data         
          Data.newtimes.push_back(interTime);
          Data.newdata.push_back(L2(Data.data2[k-1], Data.data2[k], Data.data2[k+1], Data.timeGrid[k-1], Data.timeGrid[k], Data.timeGrid[k+1], interTime));  */  //NOTE: this is the code for quadratic lagrange interpolation
          //NOTE: this do-while loop finds the largest local maximum if noise masks the true main maximum.
          searchmaxind=k;        //start at first found local maximum
          exactmaxind =k;
          olddat=Data.data2[k]; 
          if(Data.timeGrid[Data.timeGrid.size()-1] - Data.timeGrid[k] > 2.0*maxtimewindow)
           {
            do
             {
              cout << "searchmaxind= " << searchmaxind << " len data= " << Data.data2.size() << "data= " << Data.data2[searchmaxind] << " time= " << Data.timeGrid[searchmaxind] << "\n";
              searchmaxind +=1;
              if(Data.data2[searchmaxind]>olddat)
               {
                cout << "searchmaxindex= " << searchmaxind << " k= " << k << "\n";
                exactmaxind=searchmaxind;
                olddat = Data.data2[exactmaxind];
               }
             }
            while((Data.data2[searchmaxind] > mean) & (searchmaxind<Data.data2.size()-1));//timeGrid[searchmaxind] - Data.timeGrid[k]<=maxtimewindow);
            cout << "exactmaxind= " << exactmaxind << "\n";
           }
	  cout << "interpolate quintic Lagrange \n";
          if((exactmaxind>2) & (exactmaxind<=Data.data2.size()-3))//NOTE: Interpolate only if maximum allows for quintic maximum
           {
	    cout << "interpolation is possible: exactmaxind= " << exactmaxind << "\n";	   
            LAGRANGE_MAX(Data.data2[exactmaxind-3], Data.data2[exactmaxind-2], Data.data2[exactmaxind-1], Data.data2[exactmaxind], Data.data2[exactmaxind+1], Data.data2[exactmaxind+2], Data.timeGrid[exactmaxind-3],Data.timeGrid[exactmaxind-2],Data.timeGrid[exactmaxind-1],Data.timeGrid[exactmaxind],Data.timeGrid[exactmaxind+1],Data.timeGrid[exactmaxind+2], interTime, a);//NOTE: This function assumes a quintic 6-point maximum interpolation and finds the maximal value of the function and the corresponding time with bisection method. Resulting values are the maximum data and the maximum time
            cout << "intertime= "<< interTime << " a= "<< a << "\n";
            Data.newtimes.push_back(interTime);
            Data.newdata.push_back(a);    
           }
         }
        }
      }

     cout << "insert interpolated values back to data \n";
     counter=0;//NOTE: this indexes the maximum points and times, "index" indexes the new data vextor with maximum points
               //      loop to restore the time series with interpolatioin points
     for(int k=0;k<Data.data2.size();k++)
      {
	cout << "k = " << k << "\n";
	cout << Data.data2[k] << " " << Data.timeGrid[k] << "\n";
	cout << "anaphase= " << Data.anaphase[k] << "\n";
	cout << "anafreq=  " << Data.anafreq[k] << "\n";

        Data.data.push_back(Data.data2[k]);
        Data.originalData.push_back(Data.data2[k]);//save the origianl data for plotting, "data" is changed for amplitude reconstruction (planed)
        Data.Grid.push_back(Data.timeGrid[k]);
        Data.anaphase2.push_back(Data.anaphase[k]);
	Data.anafreq2.push_back(Data.anafreq[k]);
        if((Data.timeGrid[k]<Data.newtimes[counter]) & (Data.timeGrid[k+1]>Data.newtimes[counter]))
         {
	  cout << "if: tk<tmax? " << Data.timeGrid[k] << " < " << Data.newtimes[counter] << " & tk+1>tnew? " << Data.timeGrid[k+1] << " > " << Data.newtimes[counter] << "\n";
          //NOTE:Here -> if length phase is used: omit the adding of extra points to the time series but keep 
          // track of the maximum indices for cleansing purposes 
          // if length phase is not used: calculate intermediate phases arctan, length or spline. Then interpolated 
          // points are needed also there
#ifndef Phase_from_Length
          Data.data.push_back(Data.newdata[counter]);
          Data.originalData.push_back(Data.newdata[counter]);//save original data for plotting if amplitude variation
          Data.Grid.push_back(Data.newtimes[counter]); 
          Data.anaphase2.push_back(L2(Data.anaphase[k-1], Data.anaphase[k], Data.anaphase[k+1], Data.timeGrid[k-1],   Data.timeGrid[k], Data.timeGrid[k+1], Data.newtimes[counter]));
	  Data.anafreq2.push_back(L2(Data.anafreq[k-1], Data.anafreq[k], Data.anafreq[k+1], Data.timeGrid[k-1],   Data.timeGrid[k], Data.timeGrid[k+1], Data.newtimes[counter]));
          index+=1;//NOTE: shift index in new data only to get the index of the maximum point in the new data
          cout << "approached maxpoint at data index " << k << " and new data index " << index << "\n";
#endif
          counter +=1;//NOTE: go to next interpolated data value
          Data.maxindex.push_back(index);
         }
        index +=1;//NOTE: regularly shift the index in new data to next integer
       }
     cout << "length data after interpolation at maxima= " << Data.data.size() << "length maxindex after interpolation at maxima= " << Data.maxindex.size() << " length newtimes " << Data.newtimes.size() <<  "\n";
 #ifdef debuggingmaxinterpolator
   for(int k=0;k<Data.newdata.size();k++)
   {
    foutdebug2 << Data.newtimes[k] << " " << Data.newdata[k] << " " << Data.maxindex[k] << "\n";   
   }
   foutdebug2.close();
#endif
}

void SawitzkyGolayFilterQ(vector<DOUB> &data2, vector<DOUB> &time, int &WS, int &N, int &M, int d)
 {
  MatD J(WS,M);   //model matrix
  VecD f(M);      //window data
  MatD JJT(WS,WS);//linear matrix J*tranpose(J)
  VecD Jf(WS);    //rhs of least square fit
  VecD coeffs(WS);//solution of least square fit, 

  vector<DOUB> smoothed(data2.size());
  vector<DOUB> smoothed2(data2.size());
  copier(data2, smoothed);

  vector<DOUB> coefficients25(M);
  int index(0), lower(0), upper(smoothed2.size()), shift(0);
  DOUB prod(1.0);

  for(int m=0; m<N; m++)//repetition of smoothings
   {
    for(int k=0; k<smoothed2.size();k++)//all points to smooth or J.cols() cut at ends
     {
      for(int q=0;q<J.cols();q++)
       {
        shift = (-(J.cols()-1)/2+q);
        index = checkIndex(k,lower,upper,shift);
        f(q) = smoothed[index];//set data to smooth
        for(int p=0;p<J.rows();p++)
         {
          prod=1.0;
          for(int r=0;r<p;r++){prod *=(time[index]-time[k]);}//set matrix coeffs
          //J(rows,cols)
          J(p,q) = prod;
         }
       }
      JJT = J*(J.transpose());
      Jf = J*f;
      //cout << J << "\n";
      //cout << f << "\n";
      //cout << JJT <<"\n";
      //cout << Jf << "\n";
      coeffs=JJT.bdcSvd(ComputeThinU | ComputeThinV).solve(Jf);
      //cout << "Convolution result= " << coeffs[0] << " derive= " << coeffs[1] << "\n";
      if(m==N-1){smoothed2[k]=coeffs[d];}
      else{smoothed2[k]=coeffs[0];}
     }
    copier(smoothed2, smoothed);
   }
  copier(smoothed, data2);
 }
 
 void SawitzkyGolayFilter25(vector<DOUB> &data2, int &WS, int &N, int &M, DOUB in)//Sawitzky-Golay filter with 9 to 25 points
 {
#ifdef debugSavitzkyGolay
  std::stringstream debsg;   //def file names
  debsg << "SawitzkyGolay."<< in << ".dat";
  std::string namesg = debsg.str();
  ofstream foutdebugsg(namesg,ios::out);
  foutdebugsg.setf(ios::scientific);foutdebugsg.precision(15);  
#endif
  vector<DOUB> smoothed(data2.size());
  vector<DOUB> smoothed2(data2.size());
  copier(data2, smoothed);
  
  vector<DOUB> coefficients25(M);
  setConvolveSG(coefficients25, M);
  
#ifdef debugSavitzkyGolay
  cout << "after set convolves \n";
  for(int s=0; s<coefficients25.size(); s++)
   {
    foutdebugsg << "coeffs= " << coefficients25[s] << "\n";
   }
#endif
  int index(0), lower(0), upper(smoothed.size()), shift(0);
  for(int m=0; m<N; m++)//repetition of smoothings
   {
    for(int k=0; k<smoothed2.size();k++)//all points to smooth
     {
      smoothed2[k]=0.0;
      for(int n=0; n<M;n++)//coefficients25.size();n++)
       {
        shift = (-(coefficients25.size()-1)/2+n)*WS;
        index = checkIndex(k,lower,upper,shift);
        DOUB cof(coefficients25[n]);
        smoothed2[k] += cof*smoothed[index];
#ifdef debugSavitzkyGolay
        foutdebugsg << "k= " << k << " index= " << index << " coefficients25.size()= " << coefficients25.size() << " smoothed.size()= " << smoothed.size() << " coefficient25[n=" << n << "]= " << coefficients25[n] << " smoothed2[k]= " << smoothed2[k] << "\n";
#endif        
       }
     }
    copier(smoothed2, smoothed);
   }
  copier(smoothed, data2);
#ifdef debugSavitzkyGolay
  foutdebugsg.close();
  cout << "after SG filter \n";
#endif     
}
  
 void setConvolveSG(vector<DOUB> &coefficients25, int &M)
  {
   switch(M)
   {
    case(9): //choose Convolution
     {
      coefficients25[0] += 15.0/429.0;
      coefficients25[1] += -55.0/429.0;
      coefficients25[2] += 30.0/429.0;
      coefficients25[3] += 135.0/429.0;
      coefficients25[4] += 179.0/429.0;
      coefficients25[5] += 135.0/429.0;
      coefficients25[6] += 30.0/429.0;
      coefficients25[7] += -55.0/429.0;
      coefficients25[8] += 15.0/429.0;
      break;
     }
    case(11): //choose Convolution
     {
      coefficients25[0]=-18.0/429;
      coefficients25[1]=-45.0/429.0;
      coefficients25[2]=-10.0/429.0;
      coefficients25[3]=60.0/429.0;
      coefficients25[4]=120.0/429.0;
      coefficients25[5]=143.0/429.0;
      coefficients25[6]=120.0/429.0;
      coefficients25[7]=60.0/429.0;
      coefficients25[8]=-10.0/429.0;
      coefficients25[9]=-45.0/429.0;
      coefficients25[10]=18.0/429.0;
      break;
     }
    case(13): //choose Convolution
     {
      coefficients25[0]=110.0/2431.0;
      coefficients25[1]=-198.0/2431.0;
      coefficients25[2]=-160.0/2431.0;
      coefficients25[3]=110.0/2431.0;
      coefficients25[4]=390.0/2431.0;
      coefficients25[5]=600.0/2431.0;
      coefficients25[6]=677.0/2431.0;
      coefficients25[7]=600.0/2431.0;
      coefficients25[8]=390.0/2431.0;
      coefficients25[9]=110.0/2431.0;
      coefficients25[10]=-160.0/2431.0;
      coefficients25[11]=-198.0/2431.0;
      coefficients25[12]=110.0/2431.0;
      break;
     }
    case(19): //choose Convolution
     {
      coefficients25[0]=340.0/7429.0;
      coefficients25[1]=-255.0/7429.0;
      coefficients25[2]=-420.0/7429.0;
      coefficients25[3]=-290.0/7429.0;
      coefficients25[4]=18.0/7429.0;
      coefficients25[5]=405.0/7429.0;
      coefficients25[6]=790.0/7429.0;
      coefficients25[7]=1110.0/7429.0;
      coefficients25[8]=1320.0/7429.0;
      coefficients25[9]=1393.0/7429.0;
      coefficients25[10]=1320.0/7429.0;
      coefficients25[11]=1110.0/7429.0;
      coefficients25[12]=790.0/7429.0;
      coefficients25[13]=405.0/7429.0;
      coefficients25[14]=18.0/7429.0;
      coefficients25[15]=-290.0/7429.0;
      coefficients25[16]=-420.0/7429.0;
      coefficients25[17]=-255.0/7429.0;
      coefficients25[18]=340.0/7429.0;
      break;
     }
    case(25): //choose Convolution
     {
      coefficients25[0]=1265.0/30015.0;
      coefficients25[1]=-345.0/30015.0;
      coefficients25[2]=-1122.0/30015.0;
      coefficients25[3]=-1255.0/30015.0;
      coefficients25[4]=-915.0/30015.0;
      coefficients25[5]=-255.0/30015.0;
      coefficients25[6]=590.0/30015.0;
      coefficients25[7]=1503.0/30015.0;
      coefficients25[8]=2385.0/30015.0;
      coefficients25[9]=3155.0/30015.0;
      coefficients25[10]=3750.0/30015.0;
      coefficients25[11]=4125.0/30015.0;
      coefficients25[12]=4253.0/30015.0;
      coefficients25[13]=4125.0/30015.0;
      coefficients25[14]=3750.0/30015.0;
      coefficients25[15]=3155.0/30015.0;
      coefficients25[16]=2385.0/30015.0;
      coefficients25[17]=1503.0/30015.0;
      coefficients25[18]=590.0/30015.0;
      coefficients25[19]=-255.0/30015.0;
      coefficients25[20]=-915.0/30015.0;
      coefficients25[21]=-1255.0/30015.0;
      coefficients25[22]=-1122.0/30015.0;
      coefficients25[23]=-345.0/30015.0;
      coefficients25[24]=1265.0/30015.0;
      break;
     }
    }
  }
  
 int checkIndex(int &index, int &lower, int &upper, int &shift)
  {
   int newindex(index+shift);
   if(index+shift<lower)
    {
     newindex = (-1.0)*(index+shift);
    }
   if(index+shift>=upper)
    {
     newindex=upper-(index+shift-upper)-1;
    }
   return newindex;
  }
 
 void movingAverage(vector<DOUB> &data2, int &WS, int &N)
  {
   vector<DOUB> smoothed(data2.size());
   vector<DOUB> smoothed2(data2.size());
   copier(data2, smoothed);
   for(int n=0;n<N;n++)
    {     
     for(int k=0; k<WS;k++)//first points
      {
       smoothed2[k] = 0.0;
       for(int m=0;m<=2*k; m++)
        {
         smoothed2[k] += smoothed[m];         
        }
       smoothed2[k] /=(2.0*DOUB(k)+1.0);          
      }
        
     for(int k=WS; k<data2.size()-WS;k++)//all central points
      {
       smoothed2[k] = 0.0;
       for(int m=0;m<=2*WS; m++)
        {
         smoothed2[k] += smoothed[k-WS+m];
        }
       smoothed2[k] /=(2.0*DOUB(WS)+1.0);
      }
     
     for(int k=data2.size()-WS; k<data2.size();k++)//last points
      {
       smoothed2[k] = data2[k];
      }
      
     copier(smoothed2, smoothed);
    }
   copier(smoothed, data2);
  }
