// ### DEFINITION OF CLEANSING FUNCTIONS              ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //
#include "../HEADERFILES_ITHT_LENGTH/Main.h"

void FourierTrasform3(vector<DOUB> &proofGrid, vector<DOUB> &evenpart, vector<DOUB> &oddpart, vector<DOUB> &helpdata, vector<DOUB> &phase, int &start, int &end, ofstream &fouriertrans, DOUB &divider) //Vectordata *Data, int &start, int &end, ofstream &fouriertrans)
  {
#ifdef debuggcleansing
   cout << "fourier transf: def time diff \n";
   cout << "end= " << end << " start " << start << "\n";
   cout << "time end= " << proofGrid[end] << " time start " << proofGrid[start] << "\n";
   cout << "fourier transf: start summation \n";
#endif
   DOUB timeDifference1((proofGrid[end-1])), timeDifference2((proofGrid[start])), timeDifference((timeDifference1-timeDifference2));//no devision by 2.0, since Fourier coeffs are defined with *2
   
   DOUB A(0.0), B(0.0), C(0.0);
   cout <<"####################################timeDifference= "<< timeDifference << "\n"; 
   cout << "TEST THE INTEGRATION: \n";
   
   DOUB integr(0.0), t1(0.0), t2(0.0), t3(0.0);
   for(int m=1; m<evenpart.size();m++)
    {
     evenpart[m] = 0.0;
     oddpart[m] = 0.0;
#ifdef debuggcleansing
     cout << "mode " << m << " \n";
#endif   
     
     for(int k=start;k<end-1;k=k+2)//simpson integration based on lagrangian interpolation polynomial
      {
       t1=proofGrid[k];
       t2=proofGrid[k+1];
       t3=proofGrid[k+2];
       A=(helpdata[k]*cos(phase[k]*((DOUB)(m))*divider))/((t1-t2)*(t1-t3));
       B=(helpdata[k+1]*cos(phase[k+1]*((DOUB)(m))*divider))/((t2-t1)*(t2-t3));
       C=(helpdata[k+2]*cos(phase[k+2]*((DOUB)(m))*divider))/((t3-t1)*(t3-t2));
       evenpart[m] += (C-A)*(t1*t2*t3) +
                    t3*t3*t3*(C/3.0-B/6.0-A/6.0) +
                    t1*t1*t1*(C/6.0+B/6.0-A/3.0) +
                    t3*t3*(A/2.0*t2+B/2.0*t1-C/2.0*(t1+t2)) +
                    t1*t1*(A/2.0*(t2+t3)-B/2.0*t3-C/2.0*t2);
       A=(helpdata[k]*sin(phase[k]*((DOUB)(m))*divider))/((t1-t2)*(t1-t3));
       B=(helpdata[k+1]*sin(phase[k+1]*((DOUB)(m))*divider))/((t2-t1)*(t2-t3));
       C=(helpdata[k+2]*sin(phase[k+2]*((DOUB)(m))*divider))/((t3-t1)*(t3-t2));
       oddpart[m] += (C-A)*(t1*t2*t3) + //check "+" actually its a minus
                    t3*t3*t3*(C/3.0-B/6.0-A/6.0) +
                    t1*t1*t1*(C/6.0+B/6.0-A/3.0) +
                    t3*t3*(A/2.0*t2+B/2.0*t1-C/2.0*(t1+t2)) +
                    t1*t1*(A/2.0*(t2+t3)-B/2.0*t3-C/2.0*t2);
      }
     //cout << "simpsonian scheme: " << evenpart[m] << " " << oddpart[m] << "\n";
     evenpart[m] /= (0.5*timeDifference);
     oddpart[m] /= (0.5*timeDifference);
    }
   oddpart[0] = 0.0;  
   evenpart[0] = 0.0;
   for(int k=start;k<end-1;k++)
    {
     evenpart[0] += (helpdata[k+1]+helpdata[k])*(proofGrid[k+1]-proofGrid[k])*0.5;
    }
   evenpart[0]/=timeDifference;
#ifndef debuggamplitude
#ifdef debuggcleansing
   cout << "fouriertrans \n";
   for(int m=0; m<evenpart.size();m++)
    { 
     fouriertrans << m << " " << evenpart[m] << " " << oddpart[m] << "\n";
    }
#endif
#endif
#ifndef debuggcleansing
#ifdef debuggamplitude
    for(int m=0; m<evenpart.size();m++)
     {
      fouriertrans << m << " " << evenpart[m] << " " << oddpart[m] << "\n";
     }
#endif
#endif
#ifdef parametric_phase
    for(int m=0; m<evenpart.size();m++)
     {
      fouriertrans << m << " " << evenpart[m] << " " << oddpart[m] << "\n";
     }
#endif

  }
  
 DOUB TransfromSum(Vectordata *Data, DOUB x)
  {
   DOUB Newphase(0.0);
   for(int n=1; n<Data->evenpart.size(); n++)
    {
     Newphase += (Data->evenpart[n]/((DOUB)(n))*sin(((DOUB)(n))*x) - Data->oddpart[n]/((DOUB)(n))*cos(((DOUB)(n))*x) + Data->oddpart[n]/((DOUB)(n)));//before: + + - . now + - + 
    }
   return x + Newphase; 
  }
  
  void phaseTransform4(Vectordata *Data, ofstream &transform)
   {
    cout << "phase trans \n";
    DOUB strphase(0.0);
   for(int k=0; k<Data->stretchedPhase.size(); k++)
     {
      strphase = Data->stretchedPhase[k];//Data->phase[k]
      Data->stretchedPhase[k] = TransfromSum(Data, strphase);//phase[k]
#ifndef debuggamplitude
#ifdef debuggcleansing
      transform << Data->phase[k] << " " << Data->stretchedPhase[k] << "\n";
#endif
#endif
     }
   }
 
 void iterateTransform(Vectordata *Data, int &startpoint, int &endpoint, DOUB &in, int p)
  {
   DOUB divider(1.0);
   for(int k=0; k<Ncleanse; k++)
    {
#ifndef debuggamplitude
#ifdef debuggcleansing
     std::stringstream deb3;   //def file names
     deb3 << "case" << in << "HIt" << p+1 << "ftransf" << k << ".dat";
     std::string name2 = deb3.str();
     ofstream fouriertrans(name2,ios::out);
     fouriertrans.setf(ios::scientific);fouriertrans.precision(6);

     std::stringstream deb5;   //def file names
     deb5 << "case" << in << "HIt" << p+1 << "transform" << k << ".dat";
     std::string name4 = deb5.str();
     ofstream transformf(name4,ios::out);
     transformf.setf(ios::scientific);transformf.precision(6);
      
     std::stringstream deb6;
     deb6 << "case" << in << "HIt" << p+1 << "PSI" << k << ".dat";
     std::string name5 = deb6.str();
     ofstream PSI(name5,ios::out);
     PSI.setf(ios::scientific);PSI.precision(6);
#endif
#endif
#ifndef debuggamplitude
#ifndef debuggcleansing
     ofstream fouriertrans;    //def ofstreams only, if no output is wanted
     ofstream transformf;
#endif
#endif
#ifdef debuggamplitude
#ifndef debuggcleansing
     ofstream fouriertrans;
     ofstream transformf;
#endif
#endif
     FourierTrasform3(Data->proofGrid, Data->evenpart, Data->oddpart, Data->helpdata, Data->stretchedPhase, startpoint, endpoint, fouriertrans, divider);//NOTE: Data->stretchedPhase from original data

     phaseTransform4(Data, transformf);
#ifndef debuggamplitude
#ifdef debuggcleansing
     for(int m=0;m<Data->phase.size();m++)
      {
       PSI << Data->stretchedPhase[m] << " " << Data->phase[m] << " " << Data->proofGrid[m] << " " << Data->anaphase[m] << "\n";
      }
#endif
#endif
#ifndef debuggamplitude
#ifdef debuggcleansing
     transformf.close();
     fouriertrans.close();  
     PSI.close();
#endif
#endif
    }
  }

void Cleansing_Kernel_fit(Vectordata *Data, int &)
 {
  /*TODO: 
     1.1 calculate derivative of theta
     1.2 calculate omega of theta from linear fit
     2. calculate kernel density fit of derivative of data over all points of time series
     3. calculate the integral to obtain psi from theta
     */
 }

void Pdist(Vectordata &Data, int &start, int &end, DOUB &in, int p)
 {
  std::stringstream maxerr; maxerr << "Phase_distribution" << in << "." << p << ".dat";
  std::string name01 = maxerr.str(); ofstream fout1(name01,ios::out);
  fout1.setf(ios::scientific); fout1.precision(15);
  int np(end-start);          //number of discretization points for distribution
  DOUB dp(2.0*pi/(DOUB(np))); //discretization of 2pi
  vector<DOUB> phi(np);       //phase interval (0,2pi) 
  vector<DOUB> Pdp(np);       //statistical phase distribution
  vector<DOUB> PdSp(np);      //statistical stretched phase distribution
  vector<DOUB> PdF(np);       //Fourier averaged phase distribution
  vector<DOUB> PdAP(np);      //statistical real distribution
  vector<DOUB> Np(np), NSp(np), NAP(np); //normalizations for averaging
  DOUB omeg(0.0), omegAP(0.0), n(0.0), stepadjust(0.0), mod_valP(0.0), mod_valSP(0.0), mod_valAP(0.0);
  linearFit(omeg, n, Data.phase, Data.proofGrid, start, end, stepadjust);//find omega
  linearFit(omegAP, n, Data.anaphase2, Data.proofGrid, start, end, stepadjust);//find omega

  for(int k=0; k<phi.size(); k++)
   {
    phi[k] = DOUB(k)*dp+0.5*dp;//mod_valP=Data.phase[start+k]-floor(Data.phase[start+k]/(2.0*pi))*2.0*pi;
  //  Pdp[k] = 1.0 + omeg/(Data.derive_PPhase[start+k]);
   }
  //mysort(phi, Pdp);
    
  for(int k=start; k< end; k++)//find actual disctribtion of phase on 2pi
   {
    mod_valP=Data.phase[k]-floor(Data.phase[k]/(2.0*pi))*2.0*pi;
    mod_valSP=Data.stretchedPhase[k]-floor(Data.stretchedPhase[k]/(2.0*pi))*2.0*pi;
    mod_valAP=Data.anaphase2[k]-floor(Data.anaphase2[k]/(2.0*pi))*2.0*pi;
    for(int l=0;l<np;l++)
     {
      if((mod_valP>phi[l]-0.5*dp)&(mod_valP<=phi[l]+0.5*dp))
       {
	Pdp[l] += omeg/(Data.derive_PPhase[k]);
	Np[l] += 1.0;
       }
      if((mod_valSP>phi[l]-0.5*dp)&(mod_valSP<=phi[l]+0.5*dp))
       {
        PdSp[l] += omeg/(Data.derive_CPhase[k]);
	NSp[l] += 1.0;
       }
      if((mod_valAP>phi[l]-0.5*dp)&(mod_valAP<=phi[l]+0.5*dp))
       {
	//cout << "iff AP: derive AP= " << Data.derive_APhase[k] << "\n";
        PdAP[l] += omegAP/(Data.derive_APhase[k]);
	NAP[l] += 1.0;
       }
     }
   }//*/
  DOUB A(0.0), B(0.0), C(0.0);
  for(int l=0; l<np; l++)// set up the Fourier averaged phase distribution and output
   {
    for(int q=0; q<Data.oddpart.size(); q++)
     {
      PdF[l] += Data.evenpart[q]*cos(DOUB(q)*phi[l]) + Data.oddpart[q]*sin(DOUB(q)*phi[l]);
     }
     A=Pdp[l]/Np[l];
     B=PdSp[l]/NSp[l];
     C=PdAP[l]/NAP[l];
     if(Np[l] == 0.0){A=0.0;}
     if(NSp[l] == 0.0){B=0.0;}
     if(NAP[l] == 0.0){C=0.0;}
    fout1 << phi[l] << " " << A << " " << PdF[l] << " " << B << " " << C << "\n";
   }
  fout1.close();
 }	 
  
void Cleansing_for_length_phases(Vectordata &Data, DOUB &in, int p)
 {
#ifndef debuggamplitude
#ifdef debuggcleansing
  std::stringstream deb3;   //def file names
  deb3 << "case" << in << "HIt" << p+1 << "ftransf.dat";
  std::string name2 = deb3.str();
  ofstream fouriertrans(name2,ios::out);
  fouriertrans.setf(ios::scientific);fouriertrans.precision(6);

  std::stringstream deb5;   //def file names
  deb5 << "case" << in << "HIt" << p+1 << "transform.dat";
  std::string name4 = deb5.str();
  ofstream transformf(name4,ios::out);
  transformf.setf(ios::scientific);transformf.precision(6);

  std::stringstream deb6;
  deb6 << "case" << in << "HIt" << p+1 << "PSI.dat";
  std::string name5 = deb6.str();
  ofstream PSI(name5,ios::out);
  PSI.setf(ios::scientific);PSI.precision(15);
#endif
#endif
#ifndef debuggamplitude
#ifndef debuggcleansing
  ofstream fouriertrans;    //def ofstreams only, if no output is wanted
  ofstream transformf;
#endif
#endif
#ifdef debuggamplitude
#ifndef debuggcleansing
  ofstream fouriertrans;
  ofstream transformf;
#endif
#endif
  vector<DOUB> piphase;
  vector<DOUB> pitime;
  vector<DOUB> helpdat;
  DOUB counter(1.0), divider(1.0),atPh(0.0);
  int start(0), end(0);
  piphase.push_back(Data.phase[0]);
  pitime.push_back(Data.proofGrid[0]);
  for(int k=1;k<Data.phase.size();k++)//fill vector of proxiphase and interpolate exact points of 2pi in time and phase
   {
    helpdat.push_back(1.0);
    if((Data.phase[k-1]!=2.0*pi*counter)&&(Data.phase[k]>2.0*pi*counter)&&(k<Data.phase.size()-4))//interpolate point only if detected event is not to close to 2pik
     { 
      //PSI << "new cleansing if: counter= " << counter << "*2pi phase[k-1]= " << Data.phase[k-1] << " phase[k]= " << Data.phase[k] << "\n";
      helpdat.push_back(1.0);
      atPh=2.0*pi*counter;
      piphase.push_back(atPh);
      //PSI << atPh << "\n";
      pitime.push_back(L5(Data.proofGrid[k-3], Data.proofGrid[k-2], Data.proofGrid[k-1], Data.proofGrid[k], Data.proofGrid[k+1], Data.proofGrid[k+2], Data.phase[k-3], Data.phase[k-2], Data.phase[k-1], Data.phase[k],Data.phase[k+1],Data.phase[k+2],atPh));
      //PSI << "time[k-1]= " << Data.proofGrid[k-1] << " time[k]= " << Data.proofGrid[k] << " interpolated time " << (L5(Data.proofGrid[k-3], Data.proofGrid[k-2], Data.proofGrid[k-1], Data.proofGrid[k], Data.proofGrid[k+1], Data.proofGrid[k+2], Data.phase[k-3], Data.phase[k-2], Data.phase[k-1], Data.phase[k],Data.phase[k+1],Data.phase[k+2],atPh)) << "\n";
      counter +=1.0;
      if(counter==15.0){start=k;}
      if(counter==Data.maxindex.size()-15.0){end=k;}
     }
    piphase.push_back(Data.phase[k]);
    pitime.push_back(Data.proofGrid[k]);
   }
  FourierTrasform3(pitime, Data.evenpart, Data.oddpart, helpdat, piphase, start, end, fouriertrans, divider);
  fouriertrans.close();

  phaseTransform4(&Data, transformf);
#ifndef debuggamplitude
#ifdef debuggcleansing
  for(int m=0;m<pitime.size();m++)
   {
    PSI << pitime[m] << " " << piphase[m] << "\n";
   }
#endif
#endif
#ifndef debuggamplitude
#ifdef debuggcleansing
  transformf.close();
  fouriertrans.close();
  PSI.close();
#endif
#endif
 }


void Cleansing_EMD(Vectordata *Data, int &startpoint, int endpoint, DOUB &in, int p)
 {
  //NOTE: Instead of Fourier modes or a Kernel fitting, use the empiric mode-decomposition.
  //TODO: develop method
  std::stringstream maxerr; maxerr << "CL_EMD_max.dat";
  std::string name01 = maxerr.str(); ofstream fout1(name01,ios::out);
  fout1.setf(ios::scientific); fout1.precision(15);

  std::stringstream minerr; minerr << "CL_EMD_min.dat";
  std::string name02 = minerr.str(); ofstream fout2(name02,ios::out);
  fout2.setf(ios::scientific); fout2.precision(15);

  std::stringstream sigerr; sigerr << "CL_EMD_sigma.dat";
  std::string name03 = sigerr.str(); ofstream fout3(name03,ios::out);
  fout3.setf(ios::scientific); fout3.precision(15);

  //### find mean growth, calculate the derivative function ###//
  int aa(12),bb(4),cc(25);
  Calc_derivative(Data, Data->derive_PPhase, Data->phase, Data->proofGrid,aa ,bb ,cc);
  int start(0), end(Data->data.size());
  DOUB phi_0(0.0), omeg(0.0), a(0.0);
  linearFit(omeg, phi_0, Data->phase, Data->proofGrid, start, end, a);//fit linear function parameters to phase
  cout << "CL_EMD: mean_growth" << omeg << "\n";  
  
  //### calculate the oscillator density ###//
  vector<DOUB> density;
  for(int k=0;k<Data->data.size();k++){density.push_back(omeg/(Data->derive_PPhase[k]));}

  //### identify all extrema in the data set ###//
  vector<DOUB> time_min; vector<DOUB> dat_min;
  vector<DOUB> time_max; vector<DOUB> dat_max;
  time_min.push_back(Data->proofGrid[0]); dat_min.push_back(Data->data[0]);
  for(int k=1; k<Data->data.size()-1;k++)
   {
    if((Data->data[k-1]<Data->data[k])&(Data->data[k]>Data->data[k+1]))
     {time_max.push_back(Data->proofGrid[k]); dat_max.push_back(Data->data[k]);}//local maximum
    if((Data->data[k-1]>Data->data[k])&(Data->data[k]<Data->data[k+1]))
     {time_max.push_back(Data->proofGrid[k]); dat_max.push_back(Data->data[k]);}//local minimum
   }
  

 }





