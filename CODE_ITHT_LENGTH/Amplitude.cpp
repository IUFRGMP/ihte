// ###    DEF. OF AMPLITUDE RECONSTRUCTION            ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "../HEADERFILES_ITHT_LENGTH/Main.h"

DOUB LAGRANGE_N(int &degree, vector<DOUB> &newtimes, vector<DOUB> &newdata, DOUB &x)
  {
   DOUB sum(0.0);
   DOUB pj(1.0);
   for(int k=0; k<newtimes.size();k++)
    {
     pj=1.0;
     for(int m=0; m<newtimes.size();m++)
      {
       if(k!=m) pj *= ((x-newtimes[m])/(newtimes[k]-newtimes[m]));
      }
     pj *= newdata[k];
     sum += pj;
    }
   return sum;
  }

void ConstructAmplitude(Vectordata &Data, DOUB &L, DOUB &in, int &Nmodes)
 {
  cout << "Amplitude reconstruction starts...\n"; 
#ifdef debuggamplitude
  std::stringstream debsg;   //def file names
  debsg << "Amplitudereconstruct."<< in << ".dat";
  std::string namesg = debsg.str();
  ofstream foutdebugA(namesg,ios::out);
  foutdebugA.setf(ios::scientific);foutdebugA.precision(15);  
  
  std::stringstream deb3;   //def file names
  deb3 << "case" << in << "Amplitude_ftransf.dat";
  std::string name2 = deb3.str();
  ofstream fouriertrans(name2,ios::out);
  fouriertrans.setf(ios::scientific);fouriertrans.precision(6);

#else
  ofstream fouriertrans;
#endif
  
  for(int k=0; k<Data.data.size();k++)
   {
    Data.envelope[k] = sqrt(Data.data[k]*Data.data[k]+Data.hilberttrafo[k]*Data.hilberttrafo[k]);
   }
 
#ifdef debuggamplitude
foutdebugA.close();
fouriertrans.close();
#endif
 }

 void Demodulator(vector<DOUB> &Varydata, vector<DOUB> &data3, vector<DOUB> &proofGrid, vector<DOUB> &data, Vectordata &Data, DOUB &in, int Nint2, DOUB &thresh)
 {
  std::stringstream deb5; deb5 << "normalizeData" << in << ".dat";
  std::string name05 = deb5.str(); ofstream normal(name05,ios::out);
  normal.setf(ios::scientific); normal.precision(15);
  vector<DOUB> minima; int counterup(0), counterdown(0), zc(0);
  vector<DOUB> maxima; vector<int> minindex; vector<int> zercross;
  DOUB mean=0.0; //TODO: mean calculation 
  int zer(0);
  for(int k=0;k<data3.size()-1;k++) //find minima of data
   {
    data3[k] = -data[k];
    if(((data[k]<=0.0) & (data[k+1]>0.0)) || ((data[k]>=0.0) & (data[k+1]<0.0))){zercross.push_back(k);}
   }
  data3[data3.size()] = -data[data.size()];
  DOUB minD(findmax(data3,Nint2));
  for(int k=0; k<data.size();k++)
   {
    if((data[k]<minD) & (data[k-1]>data[k]) & (data[k+1]>data[k]))
     {
      minima.push_back(data[k]);
      minindex.push_back(k);
     } 
   }
  
  for(int k=0;k<data.size();k++)
   {
    if(data[k]<0.0){Varydata[k]=minima[counterdown];}
    if(data[k]>=0.0){Varydata[k]=Data.newdata[counterup];}
    if((data[k]<=0.0) & (data[k+1]>0.0)){counterup+=1;}
   }

  for(int k=0;k<data.size();k++)
   {
    normal << Data.proofGrid[k] << " " << data[k] << " " << Data.Varydata[k] << "\n";    
   }
}



