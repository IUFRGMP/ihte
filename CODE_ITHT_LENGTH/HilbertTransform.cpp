//### IMPLEMENTATION OF HILBERT TRANFROM FOR VARIABLE GRID SPACING ###//
//###                                                              ###//
//### AUTHOR: ERIK GENGEL, University of Potsdam                   ###//
//###                                                              ###//

#include "../HEADERFILES_ITHT_LENGTH/Main.h"

void Hilbert2(Vectordata *Data, int p, DOUB in) 
 {
#ifdef debugging
   std::stringstream deb3;   //def file names
   deb3 << in << "Hilberttrafo" << p+1 << ".dat";
   std::string name2 = deb3.str();
   ofstream foutdebug3(name2,ios::out);
   foutdebug3.setf(ios::scientific);foutdebug3.precision(15);
#endif
  DOUB A(0.0),B(0.0);
  //### SUBTRACT MEAN OF INITIAL SIGNAL ###//
  DOUB mean(0.0);
   for(int k=0; k<Data->data.size()-1;k++)
    {
      mean += (Data->data[k]+Data->data[k+1])*0.5*(Data->Grid[k+1]-Data->Grid[k]);
    }
  
   mean /=(Data->Grid[Data->Grid.size()-1]-Data->Grid[0]);
   cout << "Hilber trafo data mean= " << mean << "\n";
    
   for(int k=0; k<Data->data.size();k++)
    {
     Data->data[k] -= mean;
    }
for(int atj=0; atj<Data->data.size();atj++)
 { 
   Data->hilberttrafo[atj] = 0.0;
   for(int k=0;k<Data->data.size();k++) //integration loop
      {
         if((k>0) & (k<Data->data.size()-1))
          {
           if((Data->Grid[k]!=Data->Grid[atj]) & (Data->Grid[k+1]!=Data->Grid[atj]))
            {
             A=(Data->data[k]*Data->Grid[k+1]-Data->data[k+1]*Data->Grid[k])/(Data->Grid[k+1]-Data->Grid[k]);
             B=(Data->data[k+1]-Data->data[k])/(Data->Grid[k+1]-Data->Grid[k]);
             Data->hilberttrafo[atj] += B*(Data->Grid[k]-Data->Grid[k+1]) - (A + B*Data->Grid[atj])*log(abs((Data->Grid[k+1]-Data->Grid[atj])/(Data->Grid[k]-Data->Grid[atj])));
            }
           if((Data->Grid[k]==Data->Grid[atj]))
            {
             A=(Data->data[k]-Data->data[k-1])/(Data->Grid[k]-Data->Grid[k-1]);//Iatj-1
             B=(Data->data[k+1]-Data->data[k])/(Data->Grid[k+1]-Data->Grid[k]);//Iatj+1
             Data->hilberttrafo[atj] += Data->data[k]*log(abs((Data->Grid[k]-Data->Grid[k-1])/(Data->Grid[k+1]-Data->Grid[k]))) - B*(Data->Grid[k+1]-Data->Grid[k]) - A*(Data->Grid[k]-Data->Grid[k-1]);
            }
          }
         if(k==0)
          {
           if((Data->Grid[k]!=Data->Grid[atj]) & (Data->Grid[k+1]!=Data->Grid[atj]))
            {
             A=(Data->data[k]*Data->Grid[k+1]-Data->data[k+1]*Data->Grid[k])/(Data->Grid[k+1]-Data->Grid[k]);
             B=(Data->data[k+1]-Data->data[k])/(Data->Grid[k+1]-Data->Grid[k]);
             Data->hilberttrafo[atj] += B*(Data->Grid[k]-Data->Grid[k+1]) - (A + B*Data->Grid[atj])*log(abs((Data->Grid[k+1]-Data->Grid[atj])/(Data->Grid[k]-Data->Grid[atj])));
            }
          }
        }
       Data->hilberttrafo[atj] /= pi;
      }


#ifdef debugging
   for(int kk=0;kk<Data->data.size();kk++)
    {
     foutdebug3 << Data->proofGrid[kk] << " " << Data->data[kk] << " " << Data->hilberttrafo[kk] << "\n";
    }
    foutdebug3.close();
#endif
 }
 
 
void Hilbert3(Vectordata *Data, int p, DOUB in) 
 {
#ifdef debugging
   std::stringstream deb3;   //def file names
   deb3 << in << "Hilberttrafo" << p+1 << ".dat";
   std::string name2 = deb3.str();
   ofstream foutdebug3(name2,ios::out);
   foutdebug3.setf(ios::scientific);foutdebug3.precision(15);
#endif
  DOUB A(0.0),B(0.0);
  //### SUBTRACT MEAN OF INITIAL SIGNAL ###//
  DOUB mean(0.0);
  //mean=MeanCalc(Data);
  mean=MeanCalc_Length(Data);
  /*for(int k=0; k<Data->data.size()-1;k++)
   {
    mean += (Data->data[k]+Data->data[k+1])*0.5*(Data->Grid[k+1]-Data->Grid[k]);
   }
  mean /=(Data->Grid[Data->Grid.size()-1]-Data->Grid[0]);*/
  cout << "Hilber trafo data mean= " << mean << "\n";
  //### CALCULATE LOGARITHM OF DIFFERENCES ###//
  //TODO:
      
  //### atj=0 ###//
  Data->hilberttrafo[0] = 0.0;
  for(int k=1;k<Data->data.size()-1;k++)
   {
    A=((Data->data[k]-mean)*Data->Grid[k+1]-(Data->data[k+1]-mean)*Data->Grid[k])/(Data->Grid[k+1]-Data->Grid[k]);
    B=(Data->data[k+1]-Data->data[k])/(Data->Grid[k+1]-Data->Grid[k]);
    Data->hilberttrafo[0] += B*(Data->Grid[k]-Data->Grid[k+1]) - (A + B*Data->Grid[0])*log(abs((Data->Grid[k+1]-Data->Grid[0])/(Data->Grid[k]-Data->Grid[0])));
   }
  Data->hilberttrafo[0] /= pi;
  
  vector<DOUB> datai(Data->data);
  vector<DOUB> Gr(Data->Grid);
  
#pragma omp parallel for private (A,B)
  for(int atj=1; atj<Data->data.size()-1;atj++)//sequential running
    { 
     Data->hilberttrafo[atj] = 0.0;
     for(int k=0;k<atj-1;k++) //before singularity
      {
       A=((datai[k]-mean)*Gr[k+1]-(datai[k+1]-mean)*Gr[k])/(Gr[k+1]-Gr[k]);
       B=(datai[k+1]-datai[k])/(Gr[k+1]-Gr[k]);
       Data->hilberttrafo[atj] += B*(Gr[k]-Gr[k+1]) - (A + B*Gr[atj])*log(abs((Gr[k+1]-Gr[atj])/(Gr[k]-Gr[atj])));
       
      }     
     for(int k=atj+1;k<Data->data.size()-1;k++) //after singularity
      {
       A=((datai[k]-mean)*Gr[k+1]-(datai[k+1]-mean)*Gr[k])/(Gr[k+1]-Gr[k]);
       B=(datai[k+1]-datai[k])/(Gr[k+1]-Gr[k]);
       Data->hilberttrafo[atj] += B*(Gr[k]-Gr[k+1]) - (A + B*Gr[atj])*log(abs((Gr[k+1]-Gr[atj])/(Gr[k]-Gr[atj])));
      }
     //at singularity
     Data->hilberttrafo[atj] += (datai[atj]-mean)*log(abs((Gr[atj]-Gr[atj-1])/(Gr[atj+1]- Gr[atj]))) - datai[atj+1] + datai[atj-1];
     Data->hilberttrafo[atj] /= pi;
    }
  
  
  /*for(int atj=1; atj<Data->data.size()-1;atj++)//sequential running
    { 
     Data->hilberttrafo[atj] = 0.0;
     for(int k=0;k<atj-1;k++) //before singularity
      {
       A=((Data->data[k]-mean)*Data->Grid[k+1]-(Data->data[k+1]-mean)*Data->Grid[k])/(Data->Grid[k+1]-Data->Grid[k]);
       B=(Data->data[k+1]-Data->data[k])/(Data->Grid[k+1]-Data->Grid[k]);
       Data->hilberttrafo[atj] += B*(Data->Grid[k]-Data->Grid[k+1]) - (A + B*Data->Grid[atj])*log(abs((Data->Grid[k+1]-Data->Grid[atj])/(Data->Grid[k]-Data->Grid[atj])));
       
      }     
     for(int k=atj+1;k<Data->data.size()-1;k++) //after singularity
      {
       A=((Data->data[k]-mean)*Data->Grid[k+1]-(Data->data[k+1]-mean)*Data->Grid[k])/(Data->Grid[k+1]-Data->Grid[k]);
       B=(Data->data[k+1]-Data->data[k])/(Data->Grid[k+1]-Data->Grid[k]);
       Data->hilberttrafo[atj] += B*(Data->Grid[k]-Data->Grid[k+1]) - (A + B*Data->Grid[atj])*log(abs((Data->Grid[k+1]-Data->Grid[atj])/(Data->Grid[k]-Data->Grid[atj])));
      }
     //at singularity
     Data->hilberttrafo[atj] += (Data->data[atj]-mean)*log(abs((Data->Grid[atj]-Data->Grid[atj-1])/(Data->Grid[atj+1]- Data->Grid[atj]))) - Data->data[atj+1] + Data->data[atj-1];
     Data->hilberttrafo[atj] /= pi;
    }*/
   
   
    //### atj=Data->data.size()-1 ###//
  Data->hilberttrafo[Data->data.size()-1] = 0.0;
   for(int k=0;k<Data->data.size()-2;k++) //integration loop
    {
     A=((Data->data[k]-mean)*Data->Grid[k+1]-(Data->data[k+1]-mean)*Data->Grid[k])/(Data->Grid[k+1]-Data->Grid[k]);
     B=(Data->data[k+1]-Data->data[k])/(Data->Grid[k+1]-Data->Grid[k]);
     Data->hilberttrafo[Data->data.size()-1] += B*(Data->Grid[k]-Data->Grid[k+1]) - (A + B*Data->Grid[Data->data.size()-1])*log(abs((Data->Grid[k+1]-Data->Grid[Data->data.size()-1])/(Data->Grid[k]-Data->Grid[Data->data.size()-1])));
    }
   Data->hilberttrafo[Data->data.size()-1] /= pi;
   
#ifdef debugging
   for(int kk=0;kk<Data->data.size();kk++)
    {
     foutdebug3 << Data->proofGrid[kk] << " " << Data->data[kk] << " " << Data->hilberttrafo[kk] << "\n";
    }
    foutdebug3.close();
#endif
 }
 
 void Hilbert4(Vectordata *Data, int p, DOUB in) 
 {
#ifdef debugging
   std::stringstream deb3;   //def file names
   deb3 << in << "Hilberttrafo" << p+1 << ".dat";
   std::string name2 = deb3.str();
   ofstream foutdebug3(name2,ios::out);
   foutdebug3.setf(ios::scientific);foutdebug3.precision(15);
#endif
  DOUB A(0.0),B(0.0);
  //### SUBTRACT MEAN OF INITIAL SIGNAL ###//
  DOUB mean(0.0);DOUB meanit(1.0);DOUB del(0.0);
   while(abs(meanit)>=1e-9)//iterate mean of data for better approximation
    {
     for(int k=0; k<Data->data.size()-1;k++)
      {
       del=(Data->Grid[k+1]-Data->Grid[k]);
       meanit += (Data->data[k]+Data->data[k+1])*0.5*del - mean*del;
      }
     meanit /=(Data->Grid[Data->Grid.size()-1]-Data->Grid[0]);
     mean+=meanit;
     cout << "meanit= " << meanit << " mean= " << mean << "\n";
    }
   //mean /=(Data->Grid[Data->Grid.size()-1]-Data->Grid[0]);
   cout << "Hilber trafo data mean= " << mean << "\n";
    
  //### CALCULATE LOGARITHM OF DIFFERENCES ###//
  
      
  //### atj=0 ###//
  Data->hilberttrafo[0] = 0.0;
  for(int k=1;k<Data->data.size()-1;k++)
   {
    A=((Data->data[k]-mean)*Data->Grid[k+1]-(Data->data[k+1]-mean)*Data->Grid[k]);
    B=(Data->data[k+1]-Data->data[k]);
    Data->hilberttrafo[0] += Data->data[k]-Data->data[k+1] - (A + B*Data->Grid[0])*(log(abs(Data->Grid[k+1]-Data->Grid[0]))-log(abs(Data->Grid[k]-Data->Grid[0])))/(Data->Grid[k+1]-Data->Grid[k]);  
   }
  Data->hilberttrafo[0] /= pi;
  
  //### atj=1;atj<=Data->data.size()-2 ###//
  for(int atj=1; atj<Data->data.size()-1;atj++)
   { 
    Data->hilberttrafo[atj] = 0.0;
    for(int k=0;k<atj-1;k++) //before singularity
     {
      A=((Data->data[k]-mean)*Data->Grid[k+1]-(Data->data[k+1]-mean)*Data->Grid[k]);
      B=(Data->data[k+1]-Data->data[k]);
      Data->hilberttrafo[atj] += Data->data[k]-Data->data[k+1] - (A + B*Data->Grid[atj])*(log(abs(Data->Grid[k+1]-Data->Grid[atj]))-log(abs(Data->Grid[k]-Data->Grid[atj])))/(Data->Grid[k+1]-Data->Grid[k]);  
     }
    for(int k=atj+1;k<Data->data.size()-1;k++) //after singularity
     {
      A=((Data->data[k]-mean)*Data->Grid[k+1]-(Data->data[k+1]-mean)*Data->Grid[k]);
      B=(Data->data[k+1]-Data->data[k]);
      Data->hilberttrafo[atj] += Data->data[k]-Data->data[k+1] - (A + B*Data->Grid[atj])*(log(abs(Data->Grid[k+1]-Data->Grid[atj]))-log(abs(Data->Grid[k]-Data->Grid[atj])))/(Data->Grid[k+1]-Data->Grid[k]);    
     }
    //at singularity
    Data->hilberttrafo[atj] += (Data->data[atj]-mean)*(log(abs(Data->Grid[atj]-Data->Grid[atj-1])) - log(abs(Data->Grid[atj+1]-Data->Grid[atj]))) - Data->data[atj+1] + Data->data[atj-1];
    Data->hilberttrafo[atj] /= pi;
   }
   
  //### atj=Data->data.size()-1 ###//
  Data->hilberttrafo[Data->data.size()-1] = 0.0;
   for(int k=0;k<Data->data.size()-2;k++) //integration loop
    {
     A=((Data->data[k]-mean)*Data->Grid[k+1]-(Data->data[k+1]-mean)*Data->Grid[k]);
     B=(Data->data[k+1]-Data->data[k]);
     Data->hilberttrafo[Data->data.size()-1] += Data->data[k]-Data->data[k+1] - (A + B*Data->Grid[Data->data.size()-1])*(log(abs(Data->Grid[k+1]-Data->Grid[Data->data.size()-1]))-log(abs(Data->Grid[k]-Data->Grid[Data->data.size()-1])))/(Data->Grid[k+1]-Data->Grid[k]);     
    }
    
   Data->hilberttrafo[Data->data.size()-1] /= pi;
   
#ifdef debugging
   for(int kk=0;kk<Data->data.size();kk++)
    {
     foutdebug3 << Data->proofGrid[kk] << " " << Data->data[kk] << " " << Data->hilberttrafo[kk] << "\n";
    }
    foutdebug3.close();
#endif
 }

void Hilbert_Simpson(vector<DOUB> &hilberttrafo, vector<DOUB> &data, vector<DOUB> &Grid, vector<DOUB> &proofGrid, int p, DOUB in)  //NOTE: Works, Boundaries have to be treated
 {
#ifdef debugging_Hilbert_simpson
   std::stringstream deb3;   //def file names
   deb3 << in << "Hilberttrafo" << p+1 << ".dat";
   std::string name2 = deb3.str();
   ofstream foutdebug3(name2,ios::out);
   foutdebug3.setf(ios::scientific);foutdebug3.precision(15);
#endif
  DOUB A(0.0),B(0.0), C(0.0), D(0.0), E(0.0);
  //### SUBTRACT MEAN OF INITIAL SIGNAL ###//
  DOUB mean(0.0);DOUB meanit(1.0);DOUB del(0.0);
   while(abs(meanit)>=1e-9)//iterate mean of data for better approximation
    {
     for(int k=0; k<data.size()-1;k++)
      {
       del=(Grid[k+1]-Grid[k]);
       meanit += (data[k]+data[k+1])*0.5*del - mean*del;
      }
     meanit /=(Grid[Grid.size()-1]-Grid[0]);
     mean+=meanit;
     cout << "meanit= " << meanit << " mean= " << mean << "\n";
    }
   //mean /=(Data->Grid[Data->Grid.size()-1]-Data->Grid[0]);
   cout << "Hilber trafo data mean= " << mean << "\n";
    
   //### CALCULATE INTEGRAL WITH QUADRATIC INTERPOLATION ###//
   
   //atj==0
   hilberttrafo[0]=0.0;//set to zero
   for(int k=1;k<=data.size()-2;k++)
    {
     hilberttrafo[0] += data[k] - data[k+1] - log(fabs((Grid[k+1]-Grid[0])/(Grid[k]-Grid[0])))*((data[k]-mean)*Grid[k+1]-(data[k+1]-mean)*Grid[k] + Grid[0]*(data[k+1]-data[k]))/(Grid[k+1]-Grid[k]);
    }
   //integrate at singularity
   hilberttrafo[0] += -3.0*data[0]+2.0*data[1]+mean;
   hilberttrafo[0] /= pi;
   
   //### main loop ###//
   for(int atj=1;atj<data.size()-1;atj++)
   {
    //set hilberttrafo to zero
    hilberttrafo[atj]=0.0;
    //integration before sungularity
    for(int k=0;k<=atj-2;k++)
     {
      hilberttrafo[atj] += data[k] - data[k+1] - log(fabs((Grid[k+1]-Grid[atj])/(Grid[k]-Grid[atj])))*((data[k]-mean)*Grid[k+1]-(data[k+1]-mean)*Grid[k] + Grid[atj]*(data[k+1]-data[k]))/(Grid[k+1]-Grid[k]);
     }
    //integration after singularity
    for(int k=atj+1;k<=data.size()-2;k++)
     {
      hilberttrafo[atj] += data[k] - data[k+1] - log(fabs((Grid[k+1]-Grid[atj])/(Grid[k]-Grid[atj])))*((data[k]-mean)*Grid[k+1]-(data[k+1]-mean)*Grid[k] + Grid[atj]*(data[k+1]-data[k]))/(Grid[k+1]-Grid[k]);
     }
    //integration at singularity
    hilberttrafo[atj] += (data[atj]-mean)*log((Grid[atj]-Grid[atj-1])/(Grid[atj+1]-Grid[atj])) - data[atj+1] + data[atj-1];
    hilberttrafo[atj] /= pi;     
   }
   
   //atj==Data->data.size()-1
   hilberttrafo[data.size()-1]=0.0;//set to zero
    for(int k=0;k<=data.size()-1-2;k++)  //integrate all befor esingularity
     {
      hilberttrafo[data.size()-1] += data[k] - data[k+1] - log(fabs((Grid[k+1]-Grid[data.size()-1])/(Grid[k]-Grid[data.size()-1])))*((data[k]-mean)*Grid[k+1]-(data[k+1]-mean)*Grid[k] + Grid[data.size()-1]*(data[k+1]-data[k]))/(Grid[k+1]-Grid[k]);
     }
    //integrate at singularity
    hilberttrafo[data.size()-1] += 3.0*data[data.size()-1]-2.0*data[data.size()-1-1]-mean;
    hilberttrafo[data.size()-1] /= pi; 
    
#ifdef debugging_Hilbert_simpson
   for(int kk=0;kk<data.size();kk++)
    {
     foutdebug3 << proofGrid[kk] << " " << data[kk] << " " << hilberttrafo[kk] << "\n";
    }
    foutdebug3.close();
#endif
 }

void SG_derive(vector<DOUB> &hilberttrafo, vector<DOUB> &data, vector<DOUB> &Grid)
 {
  int a(5),b(4),c(25),d(2);
  copier(data,hilberttrafo);//copy data to be derivatived 
  SawitzkyGolayFilterQ(hilberttrafo, Grid, a, b, c, d);//calculate derivative
  for(int k=0;k<data.size();k++)
   {
    hilberttrafo[k] /=0.110635;
   }
 }


